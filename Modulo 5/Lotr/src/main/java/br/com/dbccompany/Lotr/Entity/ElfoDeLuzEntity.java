package br.com.dbccompany.Lotr.Entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name ="ELFO_DE_LUZ")
public class ElfoDeLuzEntity extends ElfoEntity {

	public ElfoDeLuzEntity() {
		super.setTipo(Tipo.ELFO_DE_LUZ);
	}

}
