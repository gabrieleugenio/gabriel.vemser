package br.com.dbccompany.Lotr.Entity;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DWARF_BARBALONGA")
public class DwarfBarbaLongaEntity extends DwarfEntity{

	public DwarfBarbaLongaEntity() {
		super.setTipo(Tipo.DWARF_BARBALONGA);
	}
		
}
