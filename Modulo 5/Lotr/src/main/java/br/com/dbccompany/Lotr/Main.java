package br.com.dbccompany.Lotr;

import org.hibernate.Transaction;

import org.hibernate.Session;

import br.com.dbccompany.Lotr.Entity.HibernateUtil;
import br.com.dbccompany.Lotr.Entity.PersonagemEntity;
import br.com.dbccompany.Lotr.Entity.Tipo;

public class Main {
	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			PersonagemEntity elfo = new PersonagemEntity();
			elfo.setNome("Qualquer");
			elfo.setTipo(Tipo.ELFO);
			
			session.save(elfo);
			
			transaction.commit();
		} catch (Exception e) {
			if(transaction != null) {
				transaction.rollback();
			}
			System.exit(1);
		}finally {
			System.exit(0);
		}
	}
	
//	public static void main(String[] args) {
//		Connection conn = Connector.connect();
//		try {
//			ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'PAISES'")
//				.executeQuery();
//			if (!rs.next()) {
//				conn.prepareStatement("CREATE TABLE PAISES(\n" + "ID_PAIS INTEGER PRIMARY KEY NOT NULL,\n"
//						+ "NOME VARCHAR(100) NOT NULL\n"
//						+ ")").execute();
//			}
//			PreparedStatement pst = conn.prepareStatement("INSERT INTO PAISES (ID_PAIS, NOME)" + "VALUES(SEQ_PAISES.NEXTVAL,?)");
//			pst.setString(1, "Brasil");
//			pst.executeUpdate();
//			
//			rs = conn.prepareStatement("SELECT * FROM PAISES").executeQuery();
//			while(rs.next()) {
//				System.out.println(String.format("Nome do Pais: %s",rs.getString("NOME")));
//			}
//			
//		}catch(Exception ex) {
//			Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO NA CONSULTA", ex);
//		}
//	}

}
