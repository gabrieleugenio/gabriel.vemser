package br.com.dbccompany.Cartoes.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="LANCAMENTO")
public class LancamentoEntity {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "LANCAMENTO_SEQ", sequenceName="LANCAMENTO_SEQ")
	@GeneratedValue(generator = "LANCAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
	
	@Column(name = "ID_LANCAMENTO", nullable= false)
	private Integer id;
	
	@Column(name = "DESCRICAO")
	private String descricao;
	
	@Column(name = "VALOR")
	private Double valor;
	
	@Temporal(TemporalType.DATE)
	@Column(name= "DATA_COMPRA")
	private java.util.Date data;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_EMISSOR",nullable= false)
	private EmissorEntity emissor;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_CARTAO",nullable= false)
	private CartaoEntity cartao;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_LOJA",nullable= false)
	private LojaEntity loja;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public java.util.Date getData() {
		return data;
	}

	public void setData(java.util.Date data) {
		this.data = data;
	}

	public EmissorEntity getEmissor() {
		return emissor;
	}

	public void setEmissor(EmissorEntity emissor) {
		this.emissor = emissor;
	}

	public CartaoEntity getCartao() {
		return cartao;
	}

	public void setCartao(CartaoEntity cartao) {
		this.cartao = cartao;
	}

	public LojaEntity getLoja() {
		return loja;
	}

	public void setLoja(LojaEntity loja) {
		this.loja = loja;
	}
	
	
}
