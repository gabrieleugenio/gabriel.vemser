package br.com.dbccompany.Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.dbccompany.Cartoes.Entity.LojaEntity;
import br.com.dbccompany.Cartoes.Entity.CredenciadorEntity;

@Entity
@Table(name = "LOJA_CREDENCIADOR")
@SequenceGenerator(allocationSize = 1, name = "LOJA_CREDENCIADOR_SEQ", sequenceName = "LOJA_CREDENCIADOR_SEQ")
public class LojaCredenciador {
	@Id
	@GeneratedValue(generator = "LOJA_CREDENCIADOR_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_LOJA_CREDENCIADOR",nullable= false)
	private Integer id;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "id_credenciador",
		joinColumns = { 
				@JoinColumn(name = "id_loja_credenciador")},
		inverseJoinColumns = {
				@JoinColumn(name = "id_credenciador")})
	private List<CredenciadorEntity> credenciador = new ArrayList<>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "id_loja",
		joinColumns = { 
				@JoinColumn(name = "id_loja_credenciador")},
		inverseJoinColumns = {
				@JoinColumn(name = "id_loja")})
	private List<LojaEntity> lojas = new ArrayList<>();
	
	private Double taxa;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<CredenciadorEntity> getCredenciador() {
		return credenciador;
	}

	public void setCredenciador(List<CredenciadorEntity> credenciador) {
		this.credenciador = credenciador;
	}

	public List<LojaEntity> getLojas() {
		return lojas;
	}

	public void setLojas(List<LojaEntity> lojas) {
		this.lojas = lojas;
	}

	public Double getTaxa() {
		return taxa;
	}

	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}
	

}
