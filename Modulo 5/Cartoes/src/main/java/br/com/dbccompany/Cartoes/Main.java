package br.com.dbccompany.Cartoes;

import java.util.ArrayList;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.dbccompany.Cartoes.Entity.BandeiraEntity;
import br.com.dbccompany.Cartoes.Entity.CartaoEntity;
import br.com.dbccompany.Cartoes.Entity.ClienteEntity;
import br.com.dbccompany.Cartoes.Entity.CredenciadorEntity;
import br.com.dbccompany.Cartoes.Entity.EmissorEntity;
import br.com.dbccompany.Cartoes.Entity.HibernateUtil;
import br.com.dbccompany.Cartoes.Entity.LancamentoEntity;
import br.com.dbccompany.Cartoes.Entity.LojaCredenciador;
import br.com.dbccompany.Cartoes.Entity.LojaEntity;

public class Main {

	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			LojaEntity loja = new LojaEntity();
			LojaEntity loja2 = new LojaEntity();
			loja.setNome("Renner");
			loja2.setNome("CeA");
			ArrayList<LojaEntity> lojas = new ArrayList<>();
			lojas.add(loja);
			lojas.add(loja2);
			
			CredenciadorEntity credenciador = new CredenciadorEntity();
			CredenciadorEntity credenciador2 = new CredenciadorEntity();
			credenciador.setNome("Realize");
			credenciador2.setNome("MeuCredenciador");
			ArrayList<CredenciadorEntity> credenciadores = new ArrayList<>();
			credenciadores.add(credenciador);
			credenciadores.add(credenciador2);
			
			LojaCredenciador lojaCredenciador = new LojaCredenciador();
			lojaCredenciador.setLojas(lojas);
			lojaCredenciador.setCredenciador(credenciadores);
			lojaCredenciador.setTaxa(0.6);
			
			ClienteEntity cliente = new ClienteEntity();
			cliente.setNome("Teste");
			
			BandeiraEntity bandeira = new BandeiraEntity();
			bandeira.setNome("Master");
			bandeira.setTaxa(0.5);
			
			EmissorEntity emissor = new EmissorEntity();
			emissor.setNome("Cobranca");
			emissor.setTaxa(0.3);
			
			CartaoEntity cartao = new CartaoEntity();
			cartao.setChip(123);
			cartao.setVencimento(15);
			cartao.setBandeira(bandeira);
			cartao.setCliente(cliente);
			cartao.setEmissor(emissor);
			
			LancamentoEntity lancamento = new LancamentoEntity();
			lancamento.setValor(7.49);
			lancamento.setData(new Date(2013,07,02));
			lancamento.setDescricao("Tia do lanche");
			lancamento.setCartao(cartao);
			lancamento.setEmissor(emissor);
			lancamento.setLoja(loja);
			
			
			session.save(cliente);
			session.save(loja);
			session.save(loja2);
			session.save(credenciador);
			session.save(credenciador2);
			session.save(bandeira);
			session.save(emissor);
			session.save(cartao);
			session.save(lancamento);
			session.save(lojaCredenciador);
			
			
			transaction.commit();
		} catch (Exception e) {
			if(transaction != null) {
				transaction.rollback();
			}
			System.exit(1);
		}finally {
			System.exit(0);
		}
	}

}
