function cardapioIFood(veggie = true, comLactose = false) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]
  if (!comLactose) {
    cardapio.push('pastel de queijo')
  }
  cardapio = [...cardapio, 'pastel de carne', 'empada de legumes marabijosa']
  if (veggie) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)
    cardapio.splice(cardapio.indexOf('enroladinho de salsicha'), 1)
    cardapio.splice(cardapio.indexOf('pastel de carne'), 1)
  }
  let resultadoFinal = []
  for (let i = 0; i < cardapio.length; i++) {
    resultadoFinal[i] = cardapio[i].toUpperCase()
  }
  return resultadoFinal
}
console.log(cardapioIFood()); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]
/*
spread usasse em funções onde não se sabe a totalidade de itens
cardapio = [...cardapio, 'pastel de carne', 'empada de legumes marabijosa']

correcao da linha 15 em diante ====>>>>>

let resultado = cardapio
                        .map(alimento => alimento.toUpperCase())
                        .filter(alimento => alimento.length > 12)

*/