 /* let vemSer={
    local:'DBC',
    ano:'2020',
    imprimirInformacoes:function(quantidade, modulo){
       return `Argumentos: ${this.local} | ${this.ano} | ${quantidade} | ${modulo}`;
    }
}
console.log(vemSer.imprimirInformacoes(20,3)); */

//criação de classe

/* class Jedi{
    constructor(nome){
        this.nome = nome
    }
    atacarComSabre(){
        setTimeout(() => {
                console.log(`${this.nome} atacou com sabre!`)
        }, 1000);
    }
    atacarComSabreSelf(){
        let self = this 
        setTimeout(function(){
                console.log(`${self.nome} atacou com sabre 2!`)
        }, 1000);
    }

}

/* let luke = new Jedi("Luke");
luke.atacarComSabre();
luke.atacarComSabreSelf();
console.log(luke); */

//promise, then trata o resultado de uma promessa. Só podem ser pendentes, resolvidas e rejeitada.
/* let defer = new Promise((resolve,reject) => {
    setTimeout(() => {
        if(false){
            resolve('Foi Resolvido');
        }else  {
            reject('Erro');
        }
    },2000);
});
defer
    .then((data)=>{
        console.log(data)
        return "Novo Resultado"
    })
    .then((data)=>console.log(data))
    .catch((erro)=>console.log(erro));  */ 

    let pokemon = fetch (`https://pokeapi.co/api/v2/pokemon/`);
    pokemon
        .then( data => data.json() ) 
        .then( data => console.log(data.results) );