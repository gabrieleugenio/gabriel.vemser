class Pokemon {// eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.nome = obj.name;
    this.altura = obj.height * 10;
    this.peso = obj.weight / 10;
    this.idPokemon = obj.id;
    this.imagem = obj.sprites.front_default;
    this.estatisticas = obj.stats;
    this.tipo = obj.types;
  }

  listarEstatisticas() {
    const pai = document.getElementById( 'estatisticas' );
    const filho = pai.querySelector( 'ul' );
    if ( filho !== null ) {
      filho.parentNode.removeChild( filho );
    }
    const div = document.getElementById( 'estatisticas' );
    const ul = document.createElement( 'ul' );
    div.appendChild( ul );
    ul.innerHTML = 'Estatisticas';
    for ( let i = 1; i < this.estatisticas.length; i += 1 ) {
      const li = document.createElement( 'li' );
      li.innerHTML = `${ this.estatisticas[i].stat.name } ${ this.estatisticas[i].base_stat }`;
      ul.appendChild( li );
    }
  }

  listarTipos() {
    const pai = document.getElementById( 'tipos' );
    const filho = pai.querySelector( 'ul' );
    if ( filho !== null ) {
      filho.parentNode.removeChild( filho );
    }
    const div = document.getElementById( 'tipos' );
    const ul = document.createElement( 'ul' );
    div.appendChild( ul );
    ul.innerHTML = 'Tipos:';
    for ( let i = 0; i < this.tipo.length; i += 1 ) {
      const li = document.createElement( 'li' );
      li.innerHTML = this.tipo[i].type.name;
      ul.appendChild( li );
    }
  }
}
