const sorteado = [];
let numeroPokemon;
const pokeApi = new PokeApi();
const idBusca = document.getElementById( 'inputDisplay' );
const botaoBuscar = document.getElementById( 'enviar' );
function renderizacaoPokemon( pokemon ) {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );
  const nome = dadosPokemon.querySelector( '.nome' );
  const idPokemon = dadosPokemon.querySelector( '.idPokemon' );
  const altura = dadosPokemon.querySelector( '.altura' );
  const peso = dadosPokemon.querySelector( '.peso' );
  const imagem = dadosPokemon.querySelector( '.imagem' );
  idPokemon.innerHTML = `Id: ${ pokemon.idPokemon }`;
  nome.innerHTML = `Nome: ${ pokemon.nome }`;
  altura.innerHTML = `Altura: ${ pokemon.altura } cm`;
  peso.innerHTML = `Peso: ${ pokemon.peso } Kg`;
  imagem.src = pokemon.imagem;
  pokemon.listarEstatisticas();
  pokemon.listarTipos();
}
async function buscar() {
  const pokemonEspecifico = await pokeApi.buscar( numeroPokemon );
  const poke = new Pokemon( pokemonEspecifico );
  renderizacaoPokemon( poke );
}
botaoBuscar.addEventListener( 'click', () => {
  numeroPokemon = idBusca.value;
  if ( sorteado[sorteado.length - 1] !== numeroPokemon ) {
    buscar();
    sorteado.push( numeroPokemon );
  }
} );
const sortear = document.getElementById( 'sorteador' );
sortear.addEventListener( 'click', () => {
  let i = 1;
  while ( i > 0 ) {
    numeroPokemon = Math.floor( Math.random() * 802 + 1 );
    i = 0;
    for ( let j = 0; j < sorteado.length; j += 1 ) {
      if ( sorteado[j] === numeroPokemon ) {
        i += 1;
      }
    }
  }
  buscar();
  sorteado.push( numeroPokemon );
} )
