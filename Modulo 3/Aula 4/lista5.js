let infoPokemon;
let id;
let idsUsadas = [];
function buscando() {
  
  if (id != "") {
    let pokemon = fetch(`https://pokeapi.co/api/v2/pokemon/${id}`);
    pokemon
          .then(data => data.json())
          .then(data => infoPokemon = data)
          .catch((erro) => alert('Digite um id válido')); 
    idsUsadas = [...idsUsadas,id]
    imprimirPokemon();
  }
}
let busca = document.getElementById("buscar");
busca.addEventListener("blur", function(e){
  let target = e.target;
  id = Number(target.value)
  buscando(id);
});

let buscaAleatoria = document.getElementById("botao")
buscaAleatoria.addEventListener('click',function(){
  id = Math.floor(Math.random() * 802 + 1);
  buscando(id);
});

function imprimirPokemon(){
  setTimeout(function () {
    let imagem = document.getElementById('imagem')
    imagem.src = infoPokemon.sprites.front_default;
    let nome = document.getElementById('pk');
    nome.innerHTML = "Nome: " + infoPokemon.name;
    let id = document.getElementById('id');
    id.innerHTML = "Id: " + infoPokemon.id;
    let altura = document.getElementById('altura')
    altura.innerHTML =`Altura: ${infoPokemon.height * 10}cm`
    let peso = document.getElementById('peso')
    peso.innerHTML = `Peso: ${infoPokemon.weight / 10}kg` 
    let estatisticas = document.getElementById('estatisticas')
    estatisticas.innerHTML = `Estatísticas: ${ infoPokemon.stats.map(item => item.base_stat).toString()}`
    listarTipos();
  }, 1000);
}
function listarTipos(){
  let pai = document.getElementById("tipos");
  let filho = pai.querySelector("ul");
  let filhoSpan = pai.querySelector("span");
  if (filho !== null) {
    filho.parentNode.removeChild(filho);
    filhoSpan.parentNode.removeChild(filhoSpan);
  }
  let div = document.getElementById('tipos');
  let span = document.createElement('span');
  span.innerHTML = "Tipos: "
  div.appendChild(span);
  let ul = document.createElement('ul');
  div.appendChild(ul);
  for(let i = 0; i < infoPokemon.types.length ; i++){
    let li = document.createElement('li');  
    li.innerHTML = infoPokemon.types[i].type.name;
    ul.appendChild(li);
  }
}

/* Exercício 03 - Estou com sorte!
Adicione um botão “Estou com sorte” na tela, que quando clicado, exibe um 
pokémon aleatório na tela (entre 1 e 802)
 */
