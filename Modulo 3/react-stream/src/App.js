import React from 'react';
import './App.css';
import Series from './models/series'


function App(){
  const series= new Series();
    return(
     <div className="App">
       {console.log(series.series)}
       {console.log(series.invalidas())}
       {console.log(series.filtrarPorAno(2017))}
       {console.log(series.filtrarPorNome('Anthony'))}
     </div>
     
    );
  
}
export default App