import React, { Component } from 'react'

export default class RenderizarSerie extends Component {
    render() {
        const { serie } = this.props
        return (
            <React.Fragment>
                <ul>
                    <li> <span>{serie.titulo}</span></li>
                    <li> <span>{serie.diretor}</span></li>
                    <li>{serie.anoEstreia}</li>
                    <li> {serie.invalida ? 'Série Invalida' : ''}</li>
                </ul>
            </React.Fragment>
        )
    }
}

