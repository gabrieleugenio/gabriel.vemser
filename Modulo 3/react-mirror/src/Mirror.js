import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';
import EpisodioUi from './componentes/EpisodioUi';
import MensagemFlash from './componentes/MensagemFlash';
import {Link } from 'react-router-dom';
import MeuInputNumero from './componentes/MeuInputMenu';

//import Card from './components/card';

class Mirror extends Component {
  constructor( props ) {
    super( props );
    this.listaEpisodios = new ListaEpisodios();
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      deveExibirMensagem: false,
      corExibir:true,
      msgNota:false,
      deveExibirErro:false
    }
  }
  
 /*  componentDidUpdate(_,prevState){
    if(prevState.episodio !== this.state.episodio){
      localStorage.setItem('episodio', JSON.stringify(this.state.episodio))
    }
  } */
  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState( {
      episodio
    } )
  }
  assistido() {
    const { episodio } = this.state
    //this.listaEpisodios.marcarComoAssistido( episodio )
    episodio.marcarParaAssistido()
    this.setState({
      episodio
    })
  }

  registrarNota({nota,erro}){
    this.setState({
      deveExibirErro:erro
    })
    if(erro){
      return;
    }
    const { episodio , msgNota} = this.state
    if (nota >= 1 && nota <= 5){
      episodio.avaliar( nota )
      this.setState( {
        episodio,
        deveExibirMensagem: true,
        msgNota:false
      } )
      
    }else{
      this.setState( {
        msgNota:true
      } )
    } 
  }
  geraCampoDeNota() {
    // https://reactjs.org/docs/conditional-rendering.html
    
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Qual sua nota para esse episódio?</span>
              <input type="number" placeholder="1 a 5" onBlur={ this.registrarNota.bind( this ) } ></input>
            </div>
          )
        }
      </div>
    )
  }
  atualizarMensagem = devoExibir => {
    this.setState({
      deveExibirMensagem: devoExibir,
      msgNota:devoExibir
    })
  }
  render() {
    const { episodio, deveExibirMensagem, msgNota, deveExibirErro} = this.state
    const { listaEpisodios } = this
    return (
      <div className="App">
        { /*  deveExibirMensagem ? ( <span>Registramos sua nota!</span> ) : ''  */ }
         
        <MensagemFlash atualizarMensagem={ this.atualizarMensagem }
                        deveExibirMensagem={ deveExibirMensagem} 
                         mensagem="Registramos sua nota!"/> 
         
         <MensagemFlash atualizarMensagem={ this.atualizarMensagem }
                         deveExibirMensagem={ msgNota} cor='vermelho' 
                         mensagem="Informar uma nota válida (entre 1 e 5)!"  />               
         <header className='App-header'>
           <div><Link to="/" className="button button-blue">Home</Link>
          <Link to="/jsflix" className="button button-blue">JsFlix</Link></div>
          <EpisodioUi episodio={ episodio } />
          <div>
            <button className="button" onClick={ this.sortear.bind( this ) }>Próximo</button>
            <button className="button" onClick={ this.assistido.bind( this ) }>Já Assisti</button>

            <Link to={{pathname:"/avaliacoes",state: {listaEpisodios} }} className="button button-blue">Listagem de avaliações</Link>
          </div>
          <MeuInputNumero placeholder="1 a 5"
                          mensagemCampo="Qual sua nota para esse episódio?"
                          visivel={episodio.assistido || false}
                          obrigatorio={ true } atualizarValor={this.registrarNota.bind( this )}
                           deveExibirErro={ deveExibirErro }/>
       
       
        </header>
      </div>
    );
  }
}

export default Mirror;
