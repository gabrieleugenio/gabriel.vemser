import ListaSeries from './models/ListaSeries'
import React, { Component } from 'react';
import './App.css';
import {Link } from 'react-router-dom';
import Input from './componentes/input'

export default class JsFlix  extends Component{
  constructor(props){
    super(props)
    this.listaSeries = new ListaSeries();
    this.state = {
      serieInvalida: null,
      serieFiltradaPorAno:null,
      serieProcurarPorNome:null,
      mediaDeEpisodios:null,
      valorInput:null,
      seriePorMedia:null,
      totalSalario:null,
      genero:[],
      titulo:[],
      creditos:[]
    }
  }
  seriesInvalidas(){
    const serie = this.listaSeries.invalidas();
    this.setState({
      serieInvalida : serie
    })
  }
  valorInput(evt){
    this.setState({
      valorInput: evt.target.value
    })
  }
  seriesFiltradasPorAno(){
    const { valorInput } = this.state
    let serie=this.listaSeries.filtrarPorAno(valorInput);
    serie = serie.map(serie => serie.titulo)
    this.setState({
      serieFiltradaPorAno: serie.toString()
    })
  }
  procurarSeriePorNome(){
    const { valorInput }=this.state
    
    let serie=this.listaSeries.procurarPorNome(valorInput);
    this.setState({
      serieProcurarPorNome: serie
    })
  }
  mediaDeEpisodios(){
    const serie = this.listaSeries.mediaDeEpisodios()
    this.setState({
      seriePorMedia: serie
    })
  }
  totalSalarios( ){
    const { valorInput } = this.state
    let serie = this.listaSeries.totalSalarios(valorInput);
    this.setState({
        totalSalario: serie
    })

  }
  procuraTitulo(){
    const { valorInput } = this.state
    let serie = this.listaSeries.queroTitulo(valorInput);
    serie = serie.map(serie => serie.titulo)
    this.setState({
      titulo:serie.toString()
    })
  }
  procuraGenero(){
    const { valorInput } = this.state
    let serie = this.listaSeries.queroGenero(valorInput);
    serie = serie.map(serie => serie.titulo)
    this.setState({
      genero:serie.toString()
    })
  }
  mostrarCreditos(){
    const { valorInput } = this.state
    let serie = this.listaSeries.creditos(valorInput);
    serie = serie.map(serie => serie)
    this.setState({
      creditos:serie.toString()
    })
  }
  render(){
    const { serieInvalida,serieFiltradaPorAno,serieProcurarPorNome,seriePorMedia,totalSalario,titulo,genero,creditos } = this.state 
    return(
      <div className="App-header">
       <span>
       <Link to="/" className="button button-blue">Home</Link>
        <Link to="/mirror" className="button button-blue">Mirror</Link>
      </span> 
      <Input funcao={this.seriesFiltradasPorAno.bind( this )} evento={this.valorInput.bind(this)} type={"number"} nome={'Insira o ano'}></Input>      
      <Input funcao={this.procurarSeriePorNome.bind( this )} evento={this.valorInput.bind(this)} type={'text'} nome={'Digite o nome'}></Input>     
      <button className="button button-green" onClick={ this.seriesInvalidas.bind( this ) }>Séries Inválidas</button>
      <button className="button button-green" onClick={ this.mediaDeEpisodios.bind( this ) }>Média de Séries</button>
      <Input funcao={this.totalSalarios.bind( this )} evento={this.valorInput.bind(this)} type={'number'} nome={'Total de salários'}></Input>     
      <Input funcao={this.procuraTitulo.bind( this )} evento={this.valorInput.bind(this)} type={'text'} nome={'Procurar Por titulo'}></Input>           
      <Input funcao={this.procuraGenero.bind( this )} evento={this.valorInput.bind(this)} type={'text'} nome={'Procurar Por genêro'}></Input>           
      <Input funcao={this.mostrarCreditos.bind( this )} evento={this.valorInput.bind(this)} type={'number'} nome={'Créditos'}></Input>           
      <p>{serieInvalida}</p>
      <p>{serieFiltradaPorAno}</p>
      <p>{serieProcurarPorNome ? 'Seu nome contém no elenco' : 'Nome não encontrado'}</p>
      <p>{seriePorMedia}</p>
      <p>{totalSalario}</p>
      <p>{titulo}</p>
      <p>{genero}</p>
      <p>{creditos}</p>
      </div>
    )
  }
}