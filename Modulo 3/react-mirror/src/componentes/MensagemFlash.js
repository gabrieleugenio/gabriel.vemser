import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class MensagemFlash extends Component {
    constructor(props){
        super(props)
        this.idsTimeouts = []
        this.animacao = ''
    }
    
    fechar = () => {
        console.log('fechar');
        this.props.atualizarMensagem( false )
    }
    limparTimeouts(){
        this.idsTimeouts.forEach(clearTimeout)
    }
    componentWillUnmount(){
        this.limparTimeouts()
    }
    componentDidUpdate(prevPros){
        const { deveExibirMensagem, tempo , msgNota} = this.props
        if((prevPros.deveExibirMensagem !== deveExibirMensagem)|| (prevPros.msgNota !== msgNota)){
            const novoIdTimeout = setTimeout( ()=>{
                this.fechar()
            },tempo * 1000)
            this.idsTimeouts.push( novoIdTimeout )
        }
    }
    
    render(){
        
        const { deveExibirMensagem, mensagem, cor , tempo } = this.props
        /* if(deveExibirMensagem || nota){
            setTimeout( () => {
                this.props.atualizarMensagem(false)
              }, tempo )
        } */
        if( this.animacao || deveExibirMensagem){
            this.animacao = deveExibirMensagem ?'fade-in' :'fade-out'
        }
        return (
             <span onClick={ this.fechar } className={ `flash ${cor} ${tempo} ${ this.animacao }` }>{ mensagem }</span>
        )
    }
}
MensagemFlash.propTypes = {
      mensagem: PropTypes.string.isRequired,
      deveExibirMensagem: PropTypes.bool.isRequired,
      atualizarMensagem: PropTypes.func.isRequired,
      cor: PropTypes.oneOf(['verde','vermelho']),
      tempo: PropTypes.number 
}

MensagemFlash.defaultProps = {
      cor: 'verde',
      tempo: 3
}