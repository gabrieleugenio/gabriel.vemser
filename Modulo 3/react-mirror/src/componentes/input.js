import React, { Component } from 'react';
import '../App.css';

export default class Input extends Component {
    render() {
        const { funcao, evento ,nome,tipo} = this.props
        return(
            <div>
                <input className='input' type={tipo} onChange={evento}></input>
                <button className="button" onClick={funcao}>{nome}</button>
            </div>
        )
    }
}