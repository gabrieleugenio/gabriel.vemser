import React,{ Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import JsFlix from './JsFlix';
import Mirror from './Mirror';
import ListaDeAvaliacoes from './componentes/ListaDeAvaliacoes'
import TelaDetalheEpisodio from './componentes/TelaDetalheEpisodio'

export default class App extends Component{
  
  render(){
    return(
      <Router>
        <Route path="/mirror"  component={ Mirror }/>
        <Route path="/jsflix"  component={ JsFlix }/>
        <Route path="/" exact component={ Home }/>
        <Route path="/avaliacoes" component={ ListaDeAvaliacoes }/>
        <Route path="/episodio/:id" component={ TelaDetalheEpisodio }/>
      </Router>
    );
  }
}

const Home = () => 
  <div className="App-header">
    Home
    <Link to="/mirror" className="button button-blue">Mirror</Link>
    <Link to="/jsflix" className="button button-blue button-right">JsFlix</Link>
  </div>