import ListaSeries from './models/ListaSeries'
import { Component } from 'react';
import './App.css';

export default class JsFlix  extends Component{
  constructor(props){
    super(props)
    this.listaSeries = new ListaSeries();
    console.log(this.listaSeries.invalidas());
    console.log(this.listaSeries.filtrarPorAno(2020))
    console.log(this.listaSeries.procurarPorNome('Winona Ryder'));
    console.log(this.listaSeries.mediaDeEpisodios());
    console.log(this.listaSeries.totalSalarios(1));
    console.log(this.listaSeries.queroGenero("Caos"));
    console.log(this.listaSeries.queroTitulo("The"));
    console.log(this.listaSeries.creditos(1));
  }
}