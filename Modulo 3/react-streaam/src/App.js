import React, { Component } from 'react';
import './App.css';
import Mirror from './Home'
import JsFlix from './JsFlix';


export default class App  extends Component{
  render(){
    return(
      <div className="App">
      <header className="App-header">
        <Mirror/>
        <JsFlix/>
      </header>
    </div>
    )
  }
}


