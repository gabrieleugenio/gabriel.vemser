export default class Episodio {
    constructor( nome, duracao, temporada, ordemEpisodio,thumbUrl ) {
        this.nome = nome;
        this.duracao = duracao;
        this.temporada = temporada;
        this.ordem = ordemEpisodio;
        this.url = thumbUrl;
        this.qtdVezesAssistido = 0;
    }
    avaliar( nota ){
        this.nota = parseInt( nota );
    }
    marcarParaAssistido(){
        this.assistido = true;
        this.qtdVezesAssistido += 1 
    }
  /*   marcarComoAssistido( episodio ){
        const episodioParaMarcar = this.todos.find( linha => linha.nome === episodio.nome);//find percorre todo o array a arrow function é a regra pára busca
        episodioParaMarcar.assistido = true;
    } */
    get duracaoEmMinutos( ){
        return `${ this.duracao } min.`
    }
    get temporadaEpisodio(){
        return `${ this.temporada.toString().padStart( 2, '0') }/${ this.ordem.toString().padStart( 2, '0') }`
    }
}