import React, { Component } from 'react';

export default class MensagemFlash extends Component {

    fechar = () => {
        console.log('fechar');
        this.props.atualizarMensagem( false )
    }

    render(){
        const { deveExibirMensagem, mensagem } = this.props
        return (
            <span onClick={ this.fechar } className={ `flash verde ${ deveExibirMensagem ? '' : 'invisivel' }` }>{ mensagem }</span>
        )
    }
}