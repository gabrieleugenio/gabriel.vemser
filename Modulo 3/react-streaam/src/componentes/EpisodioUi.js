import React, { Component } from 'react';

export default class EpisodioUi extends Component {
    render() {
        const { episodio } = this.props
        return(
            <React.Fragment>
                <h2>{ episodio.nome }</h2>
                <img src={ episodio.url } alt={ episodio.nome }></img>
                <span>Já assisti ? { episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido } vez(es)</span>
                <span>Temporada / Episódio: { episodio.temporadaEpisodio }</span>
                <span>Duração: { episodio.duracaoEmMinutos}</span>
                <span>Nota: { episodio.nota }</span>
            </React.Fragment>
        )
    }
}