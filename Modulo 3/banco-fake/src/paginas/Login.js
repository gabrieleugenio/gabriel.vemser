import React, { Component } from 'react';
import '../css/App.css';
import Input from '../componentes/input'
import Button from '../componentes/button'
import axios from 'axios';
import { Redirect } from 'react-router-dom'


class Login extends Component {
  constructor(props) {
    super(props)

    this.state = {
      email: '',
      senha: '',
      redirecionaParaProximaPagina: false,
    }
  }

  handleChange = (event) => {
    if (event.target.name === "senha")
      this.setState({ senha: event.target.value })

    if (event.target.name === "email")
      this.setState({ email: event.target.value })
  }

  onClickEntrar = async () => {
   
   await axios.post('http://localhost:1337/login',{email: this.state.email, senha: this.state.senha}).then(
                                                                                                    response =>{
                                                                                                      if (response.status === 200) {
                                                                            
                                                                                                        localStorage.setItem('user-autorization', response.data.token)
                                                                                                        this.setState({ redirecionaParaProximaPagina: true })
                                                                                                        if (response.status === 400) {
                                                                                                          this.setState({ redirecionaParaProximaPagina: false })
                                                                                                        } 
                                                                                                      }
      
    })
  }
  render() {

    const { redirecionaParaProximaPagina } = this.state
    
     if (redirecionaParaProximaPagina)
      return (<Redirect to={{pathname: '/central'}} /> ) 
     else
      return (
        <section className="containerHome">
           <div className="card">
             <h2 className="h2">Login</h2>
              <Input name={'email'} type={"text"} funcao={this.handleChange}  css={"input"} placeholder={"Endereço de e-mail"}/>
              <Input name={'senha'} type={"password"} funcao={this.handleChange} css={"input"} placeholder={"Senha"}/>
              <Button funcao={this.onClickEntrar} css={"button button-roxo"} nome={"Entrar"}></Button>
        </div>
        </section>
       
      );
  }
}

export default Login;
