import React, { Component } from "react";
import axios from "axios";
import Central from './Central';
import { getToken } from "./Autenticacao";
import Card from "../componentes/card";
import Input from '../componentes/input';
import MontarContasXClientes from '../componentes/montarContasXClientes'
import Button from '../componentes/button'

export default class ContasClientes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contasClientes: [],
      montarContasXClientes: false,
      clienteBusca:''
    };
  }

  componentDidMount = async () => {
    await axios
      .get("http://localhost:1337/conta/clientes", {
        headers: { Authorization: getToken() }
      })
      .then(response => {
        this.setState({ contasClientes: response.data.cliente_x_conta });
      });
  }
  pegarValor = (event) => {
    const {value} = event.target
    this.setState({
        clienteBusca: value,
        montarContasXClientes:true
    })
 }  
 verificaListagem=(e)=>{
  e.preventDefault();
  this.setState({
      montarContasXClientes:true
  })
}
  render() {
    const { contasClientes, montarContasXClientes,clienteBusca } = this.state;
    if(montarContasXClientes)
      return <MontarContasXClientes contasClientes={contasClientes} clienteBusca={clienteBusca}/>
    return (
      <Central>
        <Card>
          <span>Buscar:
        <Input placeholder={"Informe o Tipo de Conta"} funcao={this.pegarValor.bind(this)} name="contasXClientes" type="text" css="inputCard" /></span>
        <Button funcao={this.verificaListagem.bind(this)} css={"button"} nome={"Listar Todas"}/>
        </Card>
      </Central>
    );
  }
}
