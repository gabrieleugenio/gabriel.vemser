import React, { Component } from 'react';
import Button from '../componentes/button'
import axios from 'axios'
import Central from './Central'
import {getToken} from './Autenticacao'
import Card from '../componentes/card'
import MontarTipoConta from '../componentes/montarTipoConta';
import Input from '../componentes/input'
export default class TipoContas extends Component{
    constructor(props){
        super(props)
        this.state ={
            tipoContas: [],
            montarTipoConta:false,
            tipoBusca:''
        }
    }

    componentDidMount=async()=>{
     await axios.get('http://localhost:1337/tipoContas',{ headers:{'Authorization' :getToken()}} ).then(response=>{this.setState({tipoContas: response.data.tipos})})
    
    }
    pegarValor = (event) => {
        const {value} = event.target
        this.setState({
            tipoBusca: value,
            montarTipoConta:true
        })
     }  
     verificaListagem=(e)=>{
        e.preventDefault();
        this.setState({
            montarTipoConta:true
        })
    }
    render(){
        const {tipoContas, tipoBusca,montarTipoConta} =this.state
        if(montarTipoConta)
            return <MontarTipoConta tipoContas={tipoContas} tipoBusca={tipoBusca}/>
        return(
            <Central>
                <Card>
                <span>Buscar:
                <Input placeholder={"Informe Tipo de Conta"} funcao={this.pegarValor.bind(this)} name="tipoConta" type="text" css="inputCard" /></span>
                <Button funcao={this.verificaListagem.bind(this)} css={"button"} nome={"Listar Todas"}/>
                </Card>
            </Central>
        )
    }
}
