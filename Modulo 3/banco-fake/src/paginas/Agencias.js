import React, { Component } from 'react';
import axios from 'axios';
import Central from './Central';
import {getToken} from './Autenticacao';
import Card from '../componentes/card';
import MontarAgencia from '../componentes/montarAgencia';
import Input from '../componentes/input';
import Button from '../componentes/button'
export default class Agencias extends Component{
    constructor(props){
        super(props)
        this.state ={
            agencias: [],
            nomeAgencia:'', 
            montarAgencia:false,
            isDigital: false
        }
    }

    componentDidMount=()=>{
     axios.get('http://localhost:1337/agencias',{ headers:{'Authorization' :getToken()}} ).then(response=>{this.setState({agencias: response.data.agencias})})
    }
    pegarValor = (event) => {
       const {value} = event.target
       this.setState({
           nomeAgencia: value,
           montarAgencia:true
       })
    }  
    verificaListagem=(e)=>{
        e.preventDefault();
        this.setState({
            montarAgencia:true
        })
    }
    render(){
        const {agencias ,nomeAgencia,montarAgencia} =this.state
       
        if(montarAgencia)
            return <MontarAgencia agencias={ agencias} nomeAgencia={nomeAgencia}/> 
        return(
            <Central>
                <Card>
                    <span>Buscar:
                    <Input placeholder={"Nome da agência"} funcao={this.pegarValor.bind(this)} name="agencia" type="text" css="inputCard" /></span>
                    <Button funcao={this.verificaListagem.bind(this)} css={"button"} nome={"Listar Todas"}/>
                </Card>
            </Central>
        )
    }
}
