import React, { Component } from 'react';
import axios from 'axios';
import Central from './Central';
import {getToken} from './Autenticacao';
import '../css/App.css';
import Card from '../componentes/card'
import Input from '../componentes/input'
import MontarCliente from '../componentes/montarCliente';
import Button from '../componentes/button'

export default class Clientes extends Component{
    constructor(props){
        super(props)
        this.state ={
            clientes: [],
            nomePesquisa:'',
            mostrarClientes: false
        }
    }
    componentDidMount=async()=>{
     await axios.get('http://localhost:1337/clientes',{ headers:{'Authorization' :getToken()}} ).then(response=>{this.setState({clientes: response.data.clientes})})
    }
    pegarValor = (event) => {
        const {value} = event.target
        this.setState({
            nomePesquisa: value,
            mostrarClientes:true
        })
     }  
     verificaListagem=(e)=>{
        e.preventDefault();
        this.setState({
            montarAgencia:true
        })
    }
    render(){
        const {clientes, nomePesquisa, mostrarClientes} =this.state
        if(mostrarClientes)
            return <MontarCliente clientes={clientes} nomePesquisa={nomePesquisa}/>
        return(
            <Central>
                <Card>
                    <span>Buscar:
                    <Input placeholder={"Informe o nome do Cliente"} funcao={this.pegarValor.bind(this)} name="cliente" type="text" css="inputCard" /></span>
                    <Button funcao={this.verificaListagem.bind(this)} css={"button"} nome={"Listar Todas"}/>
                </Card>
            </Central>
        )
    }
}
