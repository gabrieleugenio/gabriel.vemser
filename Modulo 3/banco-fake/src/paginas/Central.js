import React, { Component } from 'react';
import '../css/reset.css'
import '../css/App.css';
import Card from '../componentes/card'
import { Link,Redirect } from 'react-router-dom'
import Button from '../componentes/button'



class Central extends Component {
    constructor(props) {
        super(props)
        this.state = {
          logout: false,
        }
      }
onClickSair = () => {
    localStorage.clear();
    this.setState({
        logout:true
    })
}
render(){
const {logout} = this.state
if(logout){
    return <Redirect to={{pathname: '/central'}} />
} 
return <div className="row">
    <div className="col col-6 col-md-3">
    <Card>
            <strong>NunConta</strong>
            <div className={'espaco'}>
            <Link to={{pathname:"/agencias"}} className={"button button-roxo"}>Agências</Link>
            <Link to={{pathname:"/clientes"}} className={"button button-roxo"}>Clientes</Link>
            <Link to={{pathname:"/tipoContas"}} className={"button button-roxo"}>Tipos de Conta</Link>
            <Link to={{pathname:"/contas/clientes"}} className={"button button-roxo"}>Contas Clientes</Link>
            </div>
            <Button funcao={this.onClickSair} css={"button"} nome={"Sair"}></Button>
    </Card>
    </div>
    <div className="col col-6 col-md-6">  
        {this.props.children}
    </div>
</div>
        
}
  
}

export default Central;
