import React, { Component } from 'react';
import axios from 'axios'
import Central from '../paginas/Central'
import {getToken} from '../paginas/Autenticacao'
import Card from './card'

export default class InfoAgencia extends Component {
    componentDidMount() {
        const { match: { params } } = this.props;
        
        axios.get(` http://localhost:1337/agencia/${params.id - 1 }`,{headers: {'Authorization' : getToken()}}).then(response =>{
        const a = response.data.agencias;
        this.setState({agencia : a})
    })}
    carregou() {
        return this.state === null ? false : true;
    }
    render () {
        return(
            <Central>
                <Card >
                {this.carregou() ?  
                <div key={this.state.agencia.id} className="linkGrande">
                    <h3>Agência:{this.state.agencia.codigo}</h3>
                    <p>{this.state.agencia.nome}</p>
                    <p> Endereço: </p>
                    <p>Logradouro: {this.state.agencia.endereco.logradouro}</p>
                    <p>Numero: {this.state.agencia.endereco.numero}</p>
                    <p>Cidade: {this.state.agencia.endereco.cidade}</p> 
                    <p>Bairro: {this.state.agencia.endereco.bairro}</p> 
                    <p>Uf: {this.state.agencia.endereco.uf}.</p>
                </div> : ''}
                </Card>
            </Central>
        );
    }
}
           