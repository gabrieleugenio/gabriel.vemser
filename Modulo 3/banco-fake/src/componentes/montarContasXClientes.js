import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Central from '../paginas/Central';
import Card from './card';
export default class MontarContasXClientes extends Component {
  
    render() {
        const {contasClientes,clienteBusca} = this.props
        let  contaCliente= contasClientes.filter( contaCliente => contaCliente.tipo.nome.toUpperCase().includes(clienteBusca.toUpperCase()))
        return(
            <Central>
                <Card>
                {contaCliente.map( contaCliente=> (
                <React.Fragment key={contaCliente.id}>
                <div className="agencia">
                    <Link to={{pathname:`/contaxcliente/${contaCliente.tipo.id}`}} className="linkGrande"><h3>Id: {contaCliente.id}</h3>
                    <p>Nome: {contaCliente.cliente.nome}</p>
                    <p>Cpf: {contaCliente.cliente.cpf}</p>
                    <p>Clique para informações de conta...</p>
                    </Link>
                </div>
            </React.Fragment>
             ))}
            </Card>
            </Central>
         )
    }
}