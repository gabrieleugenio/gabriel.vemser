import React, { Component } from 'react';
import axios from 'axios'
import Central from '../paginas/Central'
import {getToken} from '../paginas/Autenticacao'
import Card from './card'
import { Link } from 'react-router-dom'

export default class InfoCliente extends Component {
    componentDidMount() {
        const { match: { params } } = this.props;
        
        axios.get(` http://localhost:1337/cliente/${params.id - 1 }`,{headers: {'Authorization' : getToken()}}).then(response =>{
        const c = response.data.cliente;
        this.setState({cliente : c})
    })}
    carregou() {
        return this.state === null ? false : true;
    }
    render () {
        return(
            <Central>
                <Card >
                {this.carregou() ?  
                <div key={this.state.cliente.id} className="linkGrande">
                    <h3>Cliente:{this.state.cliente.id}</h3>
                   <p>Nome: {this.state.cliente.nome}</p>
                   <p>Cpf: {this.state.cliente.cpf}</p>
                   <Link to={`/agencia/${this.state.cliente.agencia.id}`} className="button button-roxo">Agência</Link>
                </div> : ''}
                </Card>
            </Central>
        );
    }
}