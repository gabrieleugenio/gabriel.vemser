import React from "react";
import "../css/reset.css";
import "../css/App.css";

export default ( props )=>
<section className="central">
<aside>
  {props.children}
</aside>
</section>