import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Central from '../paginas/Central';
import Card from './card';
export default class MontarTipoConta extends Component {
  
    render(){
        const {tipoContas, tipoBusca} =this.props
        let tipoConta = tipoContas.filter(tipoConta =>tipoConta.nome.toUpperCase().includes(tipoBusca.toUpperCase()))

        return(
            <Central>
                <Card>
                    {tipoConta.map(tipo=><React.Fragment key={tipo.id}>
                    <div className="agencia">
                    <Link to={{pathname:`/tiposContas/${tipo.id }`}} className="linkGrande">
                        <h3>Tipo:{tipo.nome}</h3>
                        <p>Id: {tipo.id}</p>
                    </Link>
                    </div>
                </React.Fragment>)} 
                </Card>
            </Central>
        )
    }
}