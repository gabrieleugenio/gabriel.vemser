import React, { Component } from 'react';
import '../css/App.css';

export default class Button extends Component {
    render(){
        const {nome, funcao,css} = this.props
        return(
            <button onClick={funcao} className={css}>{nome}</button>
        )
    }
}