import React, { Component } from 'react';
import axios from 'axios'
import Central from '../paginas/CentralCentral'
import {getToken} from '../paginas/Autenticacao'

import Card from './card'

export default class InfoContasClientes extends Component {
    componentDidMount() {
        const { match: { params } } = this.props;
        
        axios.get(` http://localhost:1337/conta/clientes/${params.id - 1 }`,{headers: {'Authorization' : getToken()}}).then(response =>{
        const cc = response.data.cliente_x_conta;
        console.log(cc)
        this.setState({contaClientes: cc })
        
    })}
    carregou() {
        return this.state === null ? false : true;
    }
    render () {
        return(
            
            <Central>
                <Card >
                {this.carregou() ? 
                    <div key={this.state.contaClientes.id} className="linkGrande">
                    <h3>Tipo da Conta: {this.state.contaClientes.tipo.nome}</h3>
                    <p>Id: {this.state.contaClientes.tipo.id}</p>
                </div> : ''}
                </Card>
            </Central>
        );
    }
}