import React, { Component } from 'react';
import axios from 'axios'
import Central from '../paginas/Central'
import {getToken} from '../paginas/Autenticacao'
import Card from './card'

export default class InfoTipoContas extends Component {
    componentDidMount() {
        const { match: { params } } = this.props;
        
        axios.get(` http://localhost:1337/tiposConta/${params.id - 1 }`,{headers: {'Authorization' : getToken()}}).then(response =>{
        const t = response.data.tipos;
        this.setState({tipo: t})
    })}
    carregou() {
        return this.state === null ? false : true;
    }
    render () {
        return(
            <Central>
                <Card >
                {this.carregou() ?  
                    <div key={this.state.tipo.id} className="linkGrande">
                    <h3>Tipo: {this.state.tipo.nome}</h3>
                    <p>Id: {this.state.tipo.id}</p>
                </div> : ''}
                </Card>
            </Central>
        );
    }
}