import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Central from '../paginas/Central';
import Card from './card';
export default class MontarAgencia extends Component {
    
    render() {
        const {nomeAgencia , agencias} = this.props
        let agencia = agencias.filter(agencia => agencia.nome.toUpperCase().includes(nomeAgencia.toUpperCase()))
        
        return(
            <Central>
                <Card>
            {agencia.map((agencia)=>
            <React.Fragment key={agencia.id}>
                <div className="agencia">
                    <Link to={{pathname:`/agencia/${agencia.id }`}} className="linkGrande"><h3>Agência:{agencia.codigo}</h3>
                        <p>Nome: {agencia.nome}</p>
                        <p>Endereço: {agencia.endereco.logradouro}</p>
                        <p>Clique e saiba mais...</p>
                    </Link>
                </div>
            </React.Fragment>)}
            </Card>
            </Central>
            )
    }
}