import React, { Component } from 'react';
import '../css/App.css';
export default class Input extends Component {
    render(){
        const {name, type, funcao,css,placeholder} = this.props
        return(
            <input name={name} type={type} onBlur={funcao} className={css} placeholder={ placeholder }/>
        )
    }
}