import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Central from '../paginas/Central';
import Card from './card';
export default class MontarCliente extends Component {
  
    render() {
        const {nomePesquisa,clientes} =this.props
        let cliente = clientes.filter((cliente) => cliente.nome.toUpperCase().includes(nomePesquisa.toUpperCase()))
        console.log(cliente)
        return(
        <Central>
            <Card>
            {cliente.map((cliente)=>
            <React.Fragment key={cliente.id}>
                <div className="agencia">
                <Link to={{pathname:`/cliente/${cliente.id}`}} className="linkGrande"><h3>Cliente:{cliente.id}</h3>
                <p>Nome: {cliente.nome}</p>
                <p>Agência:{cliente.agencia.codigo}</p>
                <p>Nome: {cliente.agencia.nome}</p>
                <p>Uf: {cliente.agencia.endereco.uf}</p>
                </Link></div>
            </React.Fragment>)}
            </Card>
    </Central>)
}
}