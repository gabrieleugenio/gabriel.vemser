import React,{ Component } from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import { PrivateRoute } from './paginas/PrivateRoute'
import Login from './paginas/Login'
import Central from './paginas/Central'
import Agencias from './paginas/Agencias'
import Clientes from './paginas/Clientes'
import TipoContas from './paginas/TipoContas'
import ContasClientes from './paginas/ContaClientes'

import InfoAgencia from './componentes/infoAgencia'
import InfoCliente from './componentes/infoCliente'
import InfoTipoContas from './componentes/infoTiposContas'
import InfoContasClientes from './componentes/infoTiposContas'

export default class App extends Component{
  constructor(props){
    super(props)
    this.state ={
      
    };
  }
  
  render(){
    return(
      <Router>
        <Route path="/" exact component={ Login }/>
        <PrivateRoute path="/central" component={ Central }/>
        <PrivateRoute path="/agencias" component={Agencias}/>
        <PrivateRoute path="/agencia/:id" component={InfoAgencia}/>
        <PrivateRoute path="/cliente/:id" component={InfoCliente}/>
        <PrivateRoute path="/tiposContas/:id" component={InfoTipoContas}/>
        <PrivateRoute path="/contaxcliente/:id" component={InfoContasClientes}/>
        <PrivateRoute path="/clientes" component={Clientes}/>
        <PrivateRoute path="/tipoContas" component={TipoContas}/> 
        <PrivateRoute path="/contas/clientes" component={ContasClientes}/> 
      </Router>
    );
  }
}

