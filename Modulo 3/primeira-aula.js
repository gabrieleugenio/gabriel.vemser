//console.log('Cheguei','aqui');
//sem o var é global nomeDaVariavel="valor";


var nomeDaVariavel = "valor";
let nomeDaLet = "ValorLet";
const nomeDaConst = "ValorConst";

//{} significa que está sendo criado um objeto
const nomeDaConst2 = {
    nome:"Gabriel",
    idade: 23
};
Object.freeze(nomeDaConst2);//congela alguma let ou const não podendo ser mais alterada
nomeDaConst2.nome = "Gabriel E."

//console.log(nomeDaConst2);

function nomeDafuncao(){
    //nomeDaVariavel ="valor";
    let nomeDaLet = "ValorLet2";
}

//console.log(nomeDaLet);


/*
function somar(){

}*/

function somar( valor1,valor2 = 1){//pode receber um valor default
    console.log(valor1 + valor2);
}
//somar(3);
//somar(3,2);

/*console.log(1 + 1);
console.log(1 + "1");*/

function ondeMoro(cidade){
    //console.log("Eu moro em " + cidade +", E sou Feliz");
    console.log(`Eu moro em ${cidade}, E sou Feliz ${30 + 1} dias.`);

}
//ondeMoro("Viamão");
function fruteira(){
    let texto = "Banana"
                + "\n"
                + "Ameixa"
                + "\n"
                +"Goiaba"
                + "\n"
                +"Pessego"
                + "\n";
    let newTexto = `
                    Banana
                    Ameixa
                    Goiaba
                    Pessego
                    `;
                    
    console.log(texto);
    console.log(newTexto);
}
//fruteira();

let eu = {
    nome: "Gabriel",
    idade: 23,
    altura: 1.75
};
function quemSou(pessoa){
    console.log(`Meu nome é ${pessoa.nome}, tenho ${pessoa.idade} e ${pessoa.altura} de altura.`);
}
//quemSou(eu);

let funcaoSomarValores = function(a , b){
    return a + b;
}
let add = funcaoSomarValores
let resultado = add(3,5)
//console.log(resultado);

const {nome:n, idade:i} = eu;
//console.log(nome,idade);
//console.log(n,i);

const array = [1,3,4,8];
const [n1,,n3,n2,n4 = 18] = array;
//console.log(n1,n2,n3,n4);

function testarPessoa({nome , idade, altura}){
    console.log(nome,idade,altura);
}
//testarPessoa(eu);

let a1 = 42;
let b1 = 15;
let c1 = 99;
let d1 = 109;
/*
let aux = a1;
a1 = b1;
b1 = aux;
*/
//console.log(a1,b1,c1,d1);

[a1,b1,c1,d1] = [b1,d1,a1,c1];

//console.log(a1,b1,c1,d1);

