import React from 'react';

export default ( props ) => 
    //só se usa quando não vai usar div
    <React.Fragment>
        { props.nome } { props.sobrenome }
    </React.Fragment>




/* [
    props.nome, 
    props.sobrenome
] */

  /*   <div>
        { props.nome } { props.sobrenome } 
    </div> */