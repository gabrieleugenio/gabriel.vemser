import React,{ Component } from 'react';
import './App.css';
import './projeto/css/layout.css'
import Header from '../src/projeto/componentes/header'
import Banner from '../src/projeto/componentes/banner'
import Footer from '../src/projeto/componentes/footer'
import CardHome from '../src/projeto/componentes/cardHome'
import Titulo from '../src/projeto/componentes/titulo'
import Texto from '../src/projeto/componentes/texto'
import img1 from '../src/projeto/img/Linhas-de-Serviço-Agil.png'
import img2 from '../src/projeto/img/Linhas-de-Serviço-DBC_Smartsourcing.png'
import img3 from '../src/projeto/img/Linhas-de-Serviço-DBC_Software-Builder.png'
import img4 from '../src/projeto/img/Linhas-de-Serviço-DBC_Sustain.png'

class Home extends Component{
    render(){
      return(
        <div>
            <Header/>
            <Banner/>
            <section className="container">
            <div className="row">
            <article className="col col-12 col-md-7">
                    <Titulo titulo={'h2'}/>
                    <Texto texto={'texto'}/>
                    <Texto texto={'texto'}/>
                </article>
                <article className="col col-12 col-md-5">
                <Titulo titulo={'h1'}/>
                <Texto texto={'texto'}/>
                </article>
            </div>
            <div className="row">
                <CardHome img={img1}/>
                <CardHome img={img2}/>
                <CardHome img={img3}/>
                <CardHome img={img4}/>
            </div>
            <Footer/>
            </section>
        </div>
      )
    }
  }
  export default Home;
