import React,{ Component } from 'react';
import '../css/layout.css'
import Titulo from './titulo'
import Texto from './texto'
export default (props) =>
    <div class="row">
        <article class="col col-12">
            <img src={props.img}/>
            <Titulo titulo={'h1'}/>
            <Texto texto={'texto'}/>
        </article>
    </div>    