import React,{ Component } from 'react';
import '../css/layout.css'
import logo from '../img/logo.png'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
export default class Header extends Component{
    render(){
        return(
            <React.Fragment>
                    <header className="main-header">
                    <nav className="container clearfix">
                        <a className="logo" href="index.html" title="Voltar à home">
                            <img src={logo} alt="DBC Company"/>
                        </a>

                        <label className="mobile-menu" for="mobile-menu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </label>
                        <input id="mobile-menu" type="checkbox"/>
                        <ul className="clearfix">
                            <li>
                                <Link to="/">Home</Link>
                            </li>
                            <li>
                                <Link to="/sobrenos">Sobre nós</Link>
                            </li>
                            <li>
                                <Link to="/servicos">Serviços</Link>
                            </li>
                            <li>
                                <Link to="/contato">Contato</Link>
                            </li>
                        </ul>
                    </nav>
                </header>
            </React.Fragment>
        )
    }
}