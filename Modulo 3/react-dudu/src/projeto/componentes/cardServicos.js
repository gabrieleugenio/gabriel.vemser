import React,{ Component } from 'react';
import '../css/layout.css'
import Button from './button'
import Titulo from './titulo'
import Texto from './texto'
export default (props) =>
    <div className="col col-12 col-md-4 col-lg-3">
        <article className="box">
            <div>
                <img src={props.img}/>
            </div>
                <Titulo titulo={'h1'}/>
                <Texto texto={'texto'}/>
        </article>
    </div>  