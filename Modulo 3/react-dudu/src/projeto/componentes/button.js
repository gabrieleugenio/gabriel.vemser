import React,{ Component } from 'react';
import '../css/layout.css'
export default (props) =>
    <a className={props.class} href={props.href}>{ props.text }</a>
          
