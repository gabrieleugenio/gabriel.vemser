import React,{ Component } from 'react';
import '../css/layout.css'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
export default class Header extends Component{
    render(){
        return(
            <React.Fragment>
                <footer class="main-footer">
                <div class="container">
                    <nav>
                    <ul className="clearfix">
                            <li>
                                <Link to="/">Home</Link>
                            </li>
                            <li>
                                <Link to="/sobrenos">Sobre nós</Link>
                            </li>
                            <li>
                                <Link to="/servicos">Serviços</Link>
                            </li>
                            <li>
                                <Link to="/contato">Contato</Link>
                            </li>
                        </ul>
                    </nav>
                    <p>
                        &copy; Copyright DBC Company - 2019
                    </p>
                </div>
                </footer>
            </React.Fragment>
        )
    }
}