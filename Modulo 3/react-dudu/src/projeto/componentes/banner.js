import React,{ Component } from 'react';
import '../css/layout.css'
import Button from './button'
import Texto from './texto'

export default class Banner extends Component{
    render(){
        return(
            <React.Fragment>
            <section className="main-banner">
                <article>
                    <h1>Vem ser DBC</h1>
                    <Texto texto={'textoG'}/>
                    <Button class={ 'button button-big button-outline' } href={'#'} text={'BANNER'}/>
                </article>
            </section>
            </React.Fragment>
        )
    }
}