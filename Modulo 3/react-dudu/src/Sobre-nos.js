import React,{ Component } from 'react';
import './App.css';
import './projeto/css/layout.css'
import Header from '../src/projeto/componentes/header'
import Banner from '../src/projeto/componentes/banner'
import Footer from '../src/projeto/componentes/footer'
import CardAbout from '../src/projeto/componentes/cardAbout'
import Titulo from '../src/projeto/componentes/titulo'
import Texto from '../src/projeto/componentes/texto'
import img1 from '../src/projeto/img/Linhas-de-Serviço-Agil.png'
import img2 from '../src/projeto/img/Linhas-de-Serviço-DBC_Smartsourcing.png'
import img3 from '../src/projeto/img/Linhas-de-Serviço-DBC_Software-Builder.png'
import img4 from '../src/projeto/img/Linhas-de-Serviço-DBC_Sustain.png'

class Sobre extends Component{
    render(){
      return(
        <div>
            <Header/>
            <section class="container about-us">
            <div className="row">
                <CardAbout img={img1}/>
                <CardAbout img={img2}/>
                <CardAbout img={img3}/>
                <CardAbout img={img4}/>
            </div>
            </section>
            <Footer/>
        </div>
      )
    }
  }
  export default Sobre;