import React,{ Component } from 'react';
import './App.css';
import './projeto/css/layout.css'
import Button from '../src/projeto/componentes/button'
import Header from '../src/projeto/componentes/header'
import Footer from '../src/projeto/componentes/footer'
import CardServicos from '../src/projeto/componentes/cardServicos'
import Titulo from '../src/projeto/componentes/titulo'
import Texto from '../src/projeto/componentes/texto'
import img1 from '../src/projeto/img/Linhas-de-Serviço-Agil.png'
import img2 from '../src/projeto/img/Linhas-de-Serviço-DBC_Smartsourcing.png'
import img3 from '../src/projeto/img/Linhas-de-Serviço-DBC_Software-Builder.png'
import img4 from '../src/projeto/img/Linhas-de-Serviço-DBC_Sustain.png'

class Servicos extends Component{
    render(){
      return(
        <div>
            <Header/>
            <section className="container centro work">
                <div className="row">
                <CardServicos img={img1}/>
                <CardServicos img={img2}/>
                <CardServicos img={img3}/>
                <CardServicos img={img4}/>
                <CardServicos img={img1}/>
                <CardServicos img={img2}/>
                </div>
                <div className="row">
                <article className="col col-12 ">
                  <Titulo titulo={'h2'}/>
                  <Texto texto={'texto'}/>
                  <Texto texto={'texto'}/>
                  <Button class={ 'button button-blue' } href={'#'} text={'Saiba Mais'}/>
                </article>
                </div>
            </section>
            <Footer/>
        </div>
      )
    }
  }
  export default Servicos;
