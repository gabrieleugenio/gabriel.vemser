import React,{ Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import Home from './Home';
import Servicos from './Servicos';
import Contato from './Contato';
import Sobre from './Sobre-nos'

export default class App extends Component{
  
  render(){
    return(
      <Router>
        <Route path="/" exact component={ Home }/>
        <Route path="/servicos" component={ Servicos }/>
        <Route path="/contato" component={ Contato }/>
        <Route path="/sobrenos" component={ Sobre }/>
      </Router>
    );
  }
}

