import React,{ Component } from 'react';
import './App.css';
import './projeto/css/layout.css'
import Header from '../src/projeto/componentes/header'
import Footer from '../src/projeto/componentes/footer'
import Titulo from '../src/projeto/componentes/titulo'
import Texto from '../src/projeto/componentes/texto'
import Input from '../src/projeto/componentes/input'
import Button from '../src/projeto/componentes/button'

class Contato extends Component{
    render(){
      return(
        <div>
            <Header/>
            <section className="container work">
                <div className="row">
                    <div className="col col-12 col-md-6 col-lg-6">
                    <form className="clearfix">
                        <Titulo titulo={'h3'}/>
                        <Texto texto={'texto'}/>
                        <Input placeholder={'Nome'}/>
                        <Input placeholder={'E-mail'}/>
                        <Input placeholder={'Assunto'}/>
                        <textarea class="field" placeholder="Mensagem"></textarea>
                        <Button class={'button button-green button-right'} href={'#'} text={'enviar'}/>
                    </form>
                    </div>
                    <div className="col col-md-6 col-lg-6">
                        <iframe className="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.7200844760855!2d-51.17087028474702!3d-30.01619288189262!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951977775fc4c071%3A0x6de693cbd6b0b5e5!2sDBC%20Company!5e0!3m2!1spt-BR!2sbr!4v1576870098547!5m2!1spt-BR!2sbr" allowfullscreen=""/>
                    </div>
                </div>
            </section>
            <Footer/>
        </div>
      )
    }
  }
  export default Contato;
