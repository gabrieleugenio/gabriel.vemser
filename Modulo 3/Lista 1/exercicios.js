/* Ex.1) Crie uma função "calcularCirculo" que receba um objeto com os seguintes
parâmetros:
{
    raio, // raio da circunferência
    tipoCalculo // se for "A" cálcula a área, se for "C" calcula a circunferência
}
*/
let circulo ={
    raio: 0,
    tipoCalculo: ""

}
circulo = {raio: 3,tipoCalculo:"C"}

function calcularCirculo({raio, tipoCalculo}){
    if(circulo.tipoCalculo == "A"){
        console.log(Math.PI * raio * raio);
    }
    if(circulo.tipoCalculo =="C"){
        console.log(2 * Math.PI * raio);
    }
}
calcularCirculo(circulo);
/*--------------------------------------------------------------------------  */

/*Ex.2)Crie uma função naoBissexto que recebe um ano (número) e verifica se ele 
não é bissexto. Exemplo:

	naoBissexto(2016) // false
	naoBissexto(2017) // true
*/

function naoBissexto(numero){
    if( ((numero - 1000) % 4 === 0) || ((numero - 1000) % 2 === 0)){
        console.log(true);
    }else{
        console.log(false);
    }
}
naoBissexto(2060);
naoBissexto(2017);
/*--------------------------------------------------------------------------- */
/*Ex.3)Crie uma função somarPares que recebe um array de números (não precisa 
fazer validação) e soma todos números nas posições pares do array, exemplo:

somarPares( [ 1, 56, 4.34, 6, -2 ] ) // 3.34 */

function somarPares(array){
    let soma = 0;
    for (let index = 0; index < array.length; index++) {
        if(index % 2 ==0){
             soma += array[index];
        }
    }
    return soma;
}
console.log(somarPares([ 1, 56, 4.34, 6, -2 ]));
/*-------------------------------------------------------------------------- */
/*Ex.4)Escreva uma função adicionar que permite somar dois números através de 
duas chamadas diferentes (não necessariamente pra mesma função). Pirou? Ex:

adicionar(3)(4) // 7
adicionar(5642)(8749) // 14391
*/
function adicionar(valor1){
    return function(valor2){
        return valor1 + valor2;
    }
   
}
//let adicionar = op1 => op2 => op1 + op2;
console.log(adicionar(3)(4));
console.log(adicionar(5642)(8749));
/*--------------------------------------------------------------------------- 
function imprimirBRL(valor){
    let val = Math.ceil(valor.toFixed(3) * 100)/100;
    return val.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
   
   
}
console.log(imprimirBRL(3498.99)); // “R$ 3.498,99”
console.log(imprimirBRL(2313477.0135)); // “R$ 2.313.477,02”
*/
/*ENCAPISULAMENTO*/
let moedas = ( function(){
    //tudo privado
    function imprimirMoeda(params){
        function arredondar(numero, precisao = 2){
            const fator = Math.pow( 10, precisao );
            return Math.ceil( numero * fator) / fator
        }
        const {
            numero,
            separadorMilhar,
            separadorDecimal,
            colocarMoeda,
            colocarNegativo
        } = params
        let qtdCasasMilhares = 3
        let StringBuffer = []
        let parteDecimal = arredondar(Math.abs(numero) % 1)
        let parteInteira = Math.trunc(numero)
        let parteInteiraString = Math.abs(parteInteira).toString()
        let parteInteiraTamanho = parteInteiraString.length
        let c = 1 
        while (parteInteiraString > 0) {
            if(c % qtdCasasMilhares == 0){
                StringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho -c)}`)
                parteInteiraString = parteInteiraString.slice(0,parteInteiraTamanho - c)
            }else if (parteInteiraString.length < qtdCasasMilhares){
                StringBuffer.push(parteInteiraString)
                parteInteiraString = ''
            }
            c++
        } 
        StringBuffer.push( parteInteiraString ) 
        let decimalString = parteDecimal.toString().replace('0.','').padStart(2,0)
        const numeroFormatado = `${StringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`
        return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(colocarMoeda(numeroFormatado));
    }
    //tudo publico
    return {
        imprimirBRL:(numero) => 
            imprimirMoeda({
                numero,
                separadorMilhar:'.',
                separadorDecimal:',',
                colocarMoeda : numeroFormatado => `R$ ${ numeroFormatado }` ,
                colocarNegativo:numeroFormatado => `-${numeroFormatado}` 
            }),
        imprimirGBP: (numero) =>
            imprimirMoeda({
                numero,
                separadorMilhar:',',
                separadorDecimal:'.',
                colocarMoeda : numeroFormatado => ` £ ${ numeroFormatado }` ,
                colocarNegativo:numeroFormatado => `-${numeroFormatado}` 
            }),
        imprimirFR: (numero) =>
            imprimirMoeda({
                numero,
                separadorMilhar:'.',
                separadorDecimal:',',
                colocarMoeda : numeroFormatado => ` ${ numeroFormatado }€` ,
                colocarNegativo:numeroFormatado => `-${numeroFormatado}` 
            })
    }
})()
console.log(moedas.imprimirGBP(2313477.0135));
console.log(moedas.imprimirGBP(-10000));
console.log(moedas.imprimirFR(10000));
console.log(moedas.imprimirFR(2313477.0135));
console.log(moedas.imprimirBRL(2313477.0135));
console.log(moedas.imprimirBRL(10000));
console.log(moedas.imprimirBRL(-2313477.0135));
console.log(moedas.imprimirBRL(-10000));
