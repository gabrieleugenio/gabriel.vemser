


import java.util.*;
public class Personagem{
   protected String nome;
   protected Status status;
   protected Inventario inventario;
   protected double vida, qtdDano,qtdVidaGanha;
   protected int experiencia, qtdXpPorAtaque;
   
   {
       status = Status.RECEM_CRIADO;
       inventario = new Inventario(0);
       experiencia = 0;
       qtdXpPorAtaque = 1;
       qtdDano = 0;
       qtdVidaGanha = 0;
   }
   public Personagem (String nome){
       this.nome = nome;
   }
   public String getNome(){
        return this.nome;
   }
   public void setNome( String nome ){
        this.nome = nome;
   }
   public double getVida(){
        return this.vida;
   }
   public Status getStatus(){
        return this.status;
   }
   public int getExperiencia(){
        return this.experiencia;
   }
   public Inventario getInventario(){
       return this.inventario;
   }
   public boolean podeSofrerDano(){
        return this.vida > 0;
   }
   public void ganharItem (Item item){
       this.inventario.adicionarItem(item);
   }
   public void perderItem(Item item){
       this.inventario.remover(item);
   }
   public void aumentarExperiencia(){
           experiencia += qtdXpPorAtaque;
   }
   public void ganharVida(){
       this.vida += this.qtdVidaGanha;
   }
   public void sofrerDano(){
        if( this.podeSofrerDano() && qtdDano > 0.0){
                this.vida = this.vida >= this.qtdDano ?
                this.vida - this.qtdDano : 0.0;
                
                if(this.getVida() == 0.0){
                     this.status = Status.MORTO;
                }else{
                     this.status = Status.SOFREU_DANO;
                }
        }
   }
}
