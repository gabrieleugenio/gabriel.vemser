

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class InventarioTest
{
    @Test
    public void adicionarItemEObterItem(){
        Inventario inventario = new Inventario(2);
        Item flecha = new Item(2,"Flecha");
        Item arco = new Item(1,"Arco");
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(arco);
        inventario.obter(0);
        inventario.getItemMaiorQtd();
        assertEquals(flecha,inventario.getItemMaiorQtd());
        assertEquals(flecha,inventario.getItens().get(0));
        assertEquals(flecha,inventario.obter(0));
    }
    @Test
    public void obterItemArrayVazio(){
        Inventario inventario = new Inventario(2);
        Item flecha = new Item(2,"Flecha");
        inventario.obter(0);
        assertEquals(null,inventario.obter(0));
    }
    @Test
    public void buscarItem(){
        Inventario inventario = new Inventario(2);
        Item flecha = new Item(2,"Flecha");
        Item arco = new Item(1,"Arco");
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(arco);
        inventario.obter(0);
        inventario.getItemMaiorQtd();
        
        assertEquals(arco,inventario.buscar("Arco"));
        assertEquals(flecha,inventario.getItemMaiorQtd());
        assertEquals(flecha,inventario.getItens().get(0));
        assertEquals(flecha,inventario.obter(0));
    }
    @Test
    public void retornaDescricoesDosItensDalista(){
        Inventario inventario = new Inventario(2);
        Item flecha = new Item(2,"Flecha");
        Item arco = new Item(1,"Arco");
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(arco);
     
        assertEquals("Flecha,Arco",inventario.getDescricoesItens());
    
    }
    @Test
    public void inverterUmaListaComDoisElementos(){
        Inventario inventario = new Inventario(2);
        Item flecha = new Item(2,"Flecha");
        Item arco = new Item(1,"Arco");
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(arco);
        
        assertEquals(arco,inventario.inverter().get(0));
        assertEquals(flecha,inventario.inverter().get(1));
        assertEquals("Flecha,Arco",inventario.getDescricoesItens());
    
    }
}
