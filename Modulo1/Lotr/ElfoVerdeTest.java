

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest
{
      @Test
    public void atiraTresFlechasTendoDuas(){
        ElfoVerde newElfo = new ElfoVerde("Legolas");
        Dwarf newDwarf = new Dwarf("Anao");
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        assertEquals(0,newElfo.getQtdFlecha());
        assertEquals(4,newElfo.getExperiencia());
        
    }
      @Test
    public void elfoVerdeGanha2XPorFlecha(){
        ElfoVerde newElfo = new ElfoVerde("Legolas");
        Dwarf newDwarf = new Dwarf("Anao");
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        assertEquals(4,newElfo.getExperiencia());
        
    }
      @Test
    public void elfoVerdeAdicionarItemComDescricaoValida(){
        ElfoVerde newElfo = new ElfoVerde("Legolas");
        Item arcoDeVidro = new Item (1,"Arco de Vidro");
        newElfo.ganharItem(arcoDeVidro);
        Inventario inventario = newElfo.getInventario();
        assertEquals(new Item(2,"Flecha"), inventario.obter(0));
        assertEquals(new Item(1,"Arco"), inventario.obter(1));
        assertEquals(arcoDeVidro, inventario.obter(2));
    }
}
