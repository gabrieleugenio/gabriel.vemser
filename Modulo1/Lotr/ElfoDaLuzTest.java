import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class ElfoDaLuzTest
{
   @Test
   public void atacarUmaVezEVerificarAvida(){
        ElfoDaLuz newElfo = new ElfoDaLuz("Legolas");
        Dwarf newDwarf = new Dwarf("Anao");
        newElfo.atacarComEspada(newDwarf);
        assertEquals(1,newElfo.getExperiencia());
        assertEquals(79.0,newElfo.getVida(),1e-9);
        assertEquals(100.0,newDwarf.getVida(),1e-9);
   }
   @Test
   public void atacarTresVezesEVerificarAvida(){
        ElfoDaLuz newElfo = new ElfoDaLuz("Legolas");
        Dwarf newDwarf = new Dwarf("Anao");
        newElfo.atacarComEspada(newDwarf);
        newElfo.atacarComEspada(newDwarf);
        newElfo.atacarComEspada(newDwarf);
        assertEquals(3,newElfo.getExperiencia());
        assertEquals(68.0,newElfo.getVida(),1e-9);
        assertEquals(80.0,newDwarf.getVida(),1e-9);
   }
   @Test
   public void verificaSeAtacaMorto(){
        ElfoDaLuz newElfo = new ElfoDaLuz("Legolas");
        Dwarf newDwarf = new Dwarf("Anao");
        //newElfo.getVida(21);
        newElfo.atacarComEspada(newDwarf);
        newElfo.atacarComEspada(newDwarf);
        newElfo.atacarComEspada(newDwarf);
        assertEquals(3,newElfo.getExperiencia());
        assertEquals(68.0,newElfo.getVida(),1e-9);
        assertEquals(80.0,newDwarf.getVida(),1e-9);
   }
}
