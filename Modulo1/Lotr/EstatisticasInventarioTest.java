

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class EstatisticasInventarioTest
{
@Test 
public void calcularMediaInventarioVazio(){
    Inventario inventario = new Inventario(1);
    EstatisticasInventario estInven = new EstatisticasInventario(inventario);
    assertTrue(Double.isNaN(estInven.calcularMedia()));
}  
@Test
public void testaMediaDeDoisItens(){
    Inventario inventario = new Inventario(2);
    Item flecha = new Item(2,"Flecha");
    Item arco = new Item(2,"Arco");
    EstatisticasInventario estInven = new EstatisticasInventario(inventario);
    inventario.adicionarItem(flecha);
    inventario.adicionarItem(arco);
    assertEquals(2.0,estInven.calcularMedia(),1e-9);
    
}
@Test
public void calculaItensAcimaDaMedia(){
        Inventario inventario = new Inventario(4);
        Item flecha = new Item(2,"Flecha");
        Item arco = new Item(2,"Arco");
        Item escudo = new Item(4,"Escudo");
        Item espada = new Item(2,"Espada");
        EstatisticasInventario estInven = new EstatisticasInventario(inventario);
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(arco);
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(espada);
        assertEquals(2.5,estInven.calcularMedia(),1e-9);
        assertEquals(1,estInven.qtdItensAcimaDaMedia());
   }
@Test
public void calculaDoisItensAcimaDaMedia(){
        Inventario inventario = new Inventario(6);
        Item flecha = new Item(2,"Flecha");
        Item arco = new Item(2,"Arco");
        Item escudo = new Item(4,"Escudo");
        Item espada = new Item(2,"Espada");
        Item lanca = new Item(1,"Lanca");
        Item machado = new Item(4,"Machado");
        EstatisticasInventario estInven = new EstatisticasInventario(inventario);
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(arco);
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(espada);
        inventario.adicionarItem(machado);
        inventario.adicionarItem(lanca);
        assertEquals(2.5,estInven.calcularMedia(),1e-9);
        assertEquals(2,estInven.qtdItensAcimaDaMedia());
   }

}
