
public class EstatisticasInventario{

private Inventario inventario;

    public EstatisticasInventario( Inventario inventario){
        this.inventario = inventario;
    }
    public double calcularMedia(){
        if(this.inventario.getItens().isEmpty()){
            return Double.NaN;// parametro nativo que retorna quando esta vazio
        }
        
        double media =0;
                for ( int i = 0; i < this.inventario.getItens().size(); i++){ 
                    // poderia usar um forit (Item item : this.inventario.getItens())
                    Item item = this.inventario.getItens().get(i);
                    if ( item != null){
                        media = media + item.getQuantidade();
                        }
            }
            return media / inventario.getItens().size();
    }
       
     public double calcularMediana(){
        if(this.inventario.getItens().isEmpty()){
            return Double.NaN;// parametro nativo que retorna quando esta vazio
        }
        int qtdItens = this.inventario.getItens().size();
        int meio = qtdItens / 2;// int quando recebe uma divisao de double arredonda para cima
        int qtdMeio = this.inventario.obter(meio).getQuantidade();
        if(qtdItens % 2 == 1){
            return qtdMeio;
        }
        int qtdMeioMenosUm = this.inventario.obter( meio - 1 ).getQuantidade();
        
        return (qtdMeio + qtdMeioMenosUm)/2.0;
      }
     public int qtdItensAcimaDaMedia(){
        double media = this.calcularMedia();
        int itemAcima = 0;
         for ( int i = 0; i < this.inventario.getItens().size(); i++){
                Item item = this.inventario.getItens().get(i);
                if ( item != null){
                    if(item.getQuantidade() > media){
                        itemAcima ++;
                    }
                }
        }
        return itemAcima;
    }
}
