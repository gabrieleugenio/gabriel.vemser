

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DwarfBarbaLongaTest
{
    @Test
    public void testaSeTomaDano(){
        DwarfBarbaLonga newDwarf = new DwarfBarbaLonga("ANAO");
        DadoFalso dadoFalso = new DadoFalso();
        newDwarf.sofrerDano();
        assertEquals(110.0,newDwarf.getVida(),1e-9);
        //assertEquals(2,dadoFalso.sortear());
    }
}
