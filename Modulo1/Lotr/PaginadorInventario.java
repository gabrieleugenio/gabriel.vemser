
/**
 
Crie uma classe chamada PaginadorInventario que tem o objetivo de organizar (paginar) os itens do inventário, que recebe um inventário na sua criação
 e possui os seguintes métodos:
pular(int): define um marcador inicial para utilizar o método abaixo
limitar(int): recebe um inteiro e retorna os n primeiros itens (n é o parâmetro deste método) a partir do marcador inicial definido no método acima, 
exemplo:

    Inventario inventario = new Inventario();
    inventario.adicionar(new Item(1, “Espada”));
    inventario.adicionar(new Item(2, “Escudo de metal”));
    inventario.adicionar(new Item(3, “Poção de HP”));
    inventario.adicionar(new Item(4, “Bracelete”));
    PaginadorInventario paginador = new PaginadorInventario(inventario);
    paginador.pular(0);
    paginador.limitar(2); // retorna os itens “Espada” e “Escudo de metal”
    paginador.pular(2);
    paginador.limitar(2); // retorna os itens “Poção de HP” e “Bracelete”

 */
import java.util.*;
public class PaginadorInventario
{
    private Inventario inventario;
    private int marcador;
    
    public PaginadorInventario( Inventario inventario){
        this.inventario = inventario;
    }
    public void pular(int marcador){
        this.marcador = marcador > 0 ? marcador : 0; 
    }
    public ArrayList<Item> limitar( int qtd ){
        ArrayList<Item> subConjunto = new ArrayList<>();
        int fim = this.marcador + qtd;
        for( int i = this.marcador; i < fim && i < this.inventario.getItens().size();i++){
            subConjunto.add(this.inventario.obter(i));
        }
        return subConjunto;
        
    }
}
