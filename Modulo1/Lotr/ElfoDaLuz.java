import java.util.ArrayList;
import java.util.Arrays;
public class ElfoDaLuz extends Elfo{
     private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
                                Arrays.asList(
                                    "Espada de Galvorn"
                                )
    );
    
    private int qtdAtaques;
    {   
        qtdAtaques = 1;
    }
    public ElfoDaLuz( String nome ){
       super(nome);
       super.ganharItem( new ItemSempreExistente(1,DESCRICOES_OBRIGATORIAS.get(0)));   
       this.qtdDano = 21.0;
       this.qtdVidaGanha = 10;
       
    }
    
    public boolean devePerderVida(){
        return qtdAtaques % 2 != 0;
    }
    
    public void atacarComEspada( Dwarf dwarf ){
        if(this.getStatus() != Status.MORTO){
            dwarf.sofrerDano();
            this.sofrerDano();
            if(devePerderVida()){
                this.sofrerDano();
            }else{
                this.ganharVida();
            }
            qtdAtaques++;
        }     
    }
    @Override
    public void perderItem( Item item){
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if(possoPerder){
            super.perderItem(item);
        }
    }
  
}
