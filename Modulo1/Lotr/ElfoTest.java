

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoTest
{
    @After
    public void tearDown(){
        System.gc();
    }
    @Test
    public void atiraTresFlechasTendoDuas(){
        Elfo newElfo = new Elfo("Legolas");
        Dwarf newDwarf = new Dwarf("Anao");
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        assertEquals(0,newElfo.getQtdFlecha());
        assertEquals(2,newElfo.getExperiencia());
    }
    @Test 
    public void cria1ElfoContadorUmaVez(){
        Elfo newElfo = new Elfo("Legolas");
        assertEquals(1, Elfo.getQtdElfos());
    }
    @Test 
    public void cria2ElfosContadorDuasVezes(){
        Elfo newElfo = new Elfo("Legolas");
        Elfo newElfo1 = new Elfo("Legolas");
        assertEquals(2, Elfo.getQtdElfos());
    }
}
