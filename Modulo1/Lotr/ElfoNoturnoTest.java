

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest
{
  @Test
  public void atiraTresFlechasTendoDuas(){
        ElfoNoturno newElfo = new ElfoNoturno("Legolas");
        Dwarf newDwarf = new Dwarf("Anao");
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        assertEquals(0,newElfo.getQtdFlecha());
  }
   @Test
  public void verificaSeGanhaExperienciaAMais(){
        ElfoNoturno newElfo = new ElfoNoturno("Legolas");
        Dwarf newDwarf = new Dwarf("Anao");
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        assertEquals(6,newElfo.getExperiencia());
  }
   @Test
  public void verificaSePerdeuVida(){
        ElfoNoturno newElfo = new ElfoNoturno("Legolas");
        Dwarf newDwarf = new Dwarf("Anao");
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        newElfo.atirarFlecha(newDwarf);
        assertEquals(70.0,newElfo.getVida(),1e-9);
  }
}
