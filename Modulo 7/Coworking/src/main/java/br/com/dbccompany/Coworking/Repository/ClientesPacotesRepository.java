package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.ClientesPacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientesPacotesRepository extends CrudRepository<ClientesPacotes,Integer> {
}
