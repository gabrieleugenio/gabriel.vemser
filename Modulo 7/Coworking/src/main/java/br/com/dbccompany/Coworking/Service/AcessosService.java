package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.Acessos;
import br.com.dbccompany.Coworking.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AcessosService {
    @Autowired
    private AcessosRepository repository;
    @Transactional(rollbackFor = Exception.class)
    public Acessos salvar(Acessos acesso){
        return repository.save(acesso);
    }
    @Transactional(rollbackFor = Exception.class)
    public Acessos editar(Acessos acesso, Integer id){
        acesso.setId(id);
        return repository.save(acesso);
    }
    public List<Acessos> todosAcessos(){
        return (List<Acessos>) repository.findAll();
    }
}

