package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.PagamentosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PagamentosRepository  extends CrudRepository<PagamentosEntity,Integer> {
}
