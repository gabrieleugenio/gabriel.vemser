package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.EspacosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacosRepository extends CrudRepository<EspacosEntity,Integer> {
}
