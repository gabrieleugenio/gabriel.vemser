package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.PagamentosEntity;
import br.com.dbccompany.Coworking.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PagamentosService {
    @Autowired
    private PagamentosRepository repository;
    @Transactional(rollbackFor = Exception.class)
    public PagamentosEntity salvar(PagamentosEntity pagamento){
        return repository.save(pagamento);
    }
    @Transactional(rollbackFor = Exception.class)
    public PagamentosEntity editar(PagamentosEntity pagamento,Integer id){
        pagamento.setId(id);
        return repository.save(pagamento);
    }
    public List<PagamentosEntity> todosPagamentos(){
        return (List<PagamentosEntity>) repository.findAll();
    }
}
