package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.EspacosEntity;
import br.com.dbccompany.Coworking.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacosService {
    @Autowired
    private EspacosRepository repository;
    @Transactional(rollbackFor = Exception.class)
    public EspacosEntity salvar(EspacosEntity espaco){
        return repository.save(espaco);
    }
    @Transactional(rollbackFor = Exception.class)
    public EspacosEntity editar(EspacosEntity espaco,Integer id){
        espaco.setId(id);
        return repository.save(espaco);
    }
    public List<EspacosEntity> todosEspacos(){
        return (List<EspacosEntity>) repository.findAll();
    }

}
