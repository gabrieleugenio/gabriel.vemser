package br.com.dbccompany.Coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ESPACOS")
public class EspacosEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACOS_SEQ", sequenceName="ESPACOS_SEQ")
    @GeneratedValue(generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ESPACOS", nullable= false)
    private Integer id;
    @Column(name = "NOME", unique = true,nullable = false)
    private String nome;
    @Column(name = "QTD_PESSOAS",nullable = false)
    private Integer qtdPessoas;
    @Column(name = "VALOR",nullable = false)
    private String valor;
    @OneToMany(mappedBy = "espacos")
    private  List<SaldoCliente> saldoClientes = new ArrayList<>();
    @OneToMany(mappedBy = "espacos")
    private List<EspacosPacotes> espacosPacotes = new ArrayList<>();

    public List<SaldoCliente> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(List<SaldoCliente> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }

    public List<EspacosPacotes> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotes> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public void setValor(String valor) {
        String brl = "R$";
        this.valor = brl.concat(valor);
    }
}
