package br.com.dbccompany.Coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CONTRATACAO")
public class ContratacaoEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName="CONTRATACAO_SEQ")
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CONTRATACAO", nullable= false)
    private Integer id;
    @Column(name = "QUANTIDADE",nullable = false)
    private Double quantidade;

    @Column(name = "DESCONTO")
    private Double desconto;

    @Column(name = "PRAZO",nullable = false)
    private Integer prazo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_CLIENTES", nullable= false)
    private ClientesEntity cliente;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_ESPACOS", nullable= false)
    private EspacosEntity espaco;

    @OneToMany(mappedBy = "contratacao")
    private List<PagamentosEntity> pagamentos = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }
}

