package br.com.dbccompany.Coworking.Service;


import br.com.dbccompany.Coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.Coworking.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContratacaoService {
    @Autowired
    private ContratacaoRepository repository;
    @Transactional(rollbackFor = Exception.class)
    public ContratacaoEntity salvar(ContratacaoEntity contratacao){
        return repository.save(contratacao);
    }
    @Transactional(rollbackFor = Exception.class)
    public ContratacaoEntity editar(ContratacaoEntity contratacao,Integer id){
        contratacao.setId(id);
        return repository.save(contratacao);
    }
    public List<ContratacaoEntity> todasContratacoes(){
        return (List<ContratacaoEntity>) repository.findAll();
    }
}
