package br.com.dbccompany.Coworking.Controller;


import br.com.dbccompany.Coworking.Entity.Usuario;
import br.com.dbccompany.Coworking.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuarios")
public class UsuarioController {
    @Autowired
    UsuarioService service;
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Usuario> usuario(){
        return service.todosUsuarios();
    }
    @PostMapping(value = "/adicionar")
    @ResponseBody
    public  Usuario novoUsuario(@RequestBody  Usuario usuario){
        return service.salvar(usuario);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public  Usuario editar(@PathVariable Integer id, @RequestBody  Usuario usuario){
        usuario.setId(id);
        return service.editar(usuario, id);
    }
}
