package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.EspacosPacotes;
import br.com.dbccompany.Coworking.Repository.EspacosPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacosPacotesService {
    @Autowired
    private EspacosPacotesRepository repository;
    @Transactional(rollbackFor = Exception.class)
    public EspacosPacotes salvar(EspacosPacotes espacosPacotes){
        return repository.save(espacosPacotes);
    }
    @Transactional(rollbackFor = Exception.class)
    public EspacosPacotes editar(EspacosPacotes espacosPacotes,Integer id){
        espacosPacotes.setId(id);
        return repository.save(espacosPacotes);
    }
    public List<EspacosPacotes> todosEspacosPacotes(){
        return (List<EspacosPacotes>) repository.findAll();
    }
}
