package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.ClientesEntity;
import br.com.dbccompany.Coworking.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientes")
public class ClientesController {
    @Autowired
    ClientesService service;
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ClientesEntity> cliente(){
        return service.todosClientes();
    }
    @PostMapping(value = "/adicionar")
    @ResponseBody
    public ClientesEntity novoCliente(@RequestBody ClientesEntity cliente){
        return service.salvar(cliente);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClientesEntity editar(@PathVariable Integer id, @RequestBody ClientesEntity cliente){
        cliente.setId(id);
        return service.editar(cliente, id);
    }

}
