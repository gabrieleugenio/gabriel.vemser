package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.EspacosPacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacosPacotesRepository extends CrudRepository<EspacosPacotes,Integer> {
}
