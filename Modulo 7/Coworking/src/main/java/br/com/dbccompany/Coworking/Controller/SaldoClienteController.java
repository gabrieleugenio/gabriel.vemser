package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.SaldoCliente;
import br.com.dbccompany.Coworking.Entity.SaldoClienteId;
import br.com.dbccompany.Coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/saldoClientes")
public class SaldoClienteController {

    @Autowired
    SaldoClienteService service;
    @GetMapping(value = "/todas")
    @ResponseBody
    public List<SaldoCliente> saldoClientes(){
       return service.todosSaldosClientes();
    }
    @PostMapping(value = "/adicionar")
    @ResponseBody
    public SaldoCliente novoCliente(@RequestBody SaldoCliente saldoCliente){
        return service.salvar(saldoCliente);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public SaldoCliente editar(@PathVariable SaldoClienteId id, @RequestBody SaldoCliente saldoCliente){
        saldoCliente.setId(id);
        return service.editar(saldoCliente, id);
    }
}
