package br.com.dbccompany.Coworking.Service;


import br.com.dbccompany.Coworking.Entity.Usuario;
import br.com.dbccompany.Coworking.Repository.UsuarioRepository;
import br.com.dbccompany.Coworking.Security.Criptografar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Usuario salvar(Usuario usuario){
        usuario.setSenha(Criptografar.criptografar(usuario.getSenha()));
        return repository.save(usuario);
    }

    @Transactional(rollbackFor = Exception.class)
    public Usuario editar(Usuario usuario,Integer id){
        usuario.setId(id);
        usuario.setSenha(Criptografar.criptografar(usuario.getSenha()));
        return repository.save(usuario);
    }
    public List<Usuario> todosUsuarios(){
        return (List<Usuario>) repository.findAll();
    }
}
