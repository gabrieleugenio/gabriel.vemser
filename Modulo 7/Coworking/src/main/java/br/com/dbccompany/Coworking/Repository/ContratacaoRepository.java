package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.ContratacaoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity,Integer> {
}
