package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.EspacosPacotes;
import br.com.dbccompany.Coworking.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/espacosPacotes")
public class EspacosPacotesController {
    @Autowired
    EspacosPacotesService service;
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<EspacosPacotes> todosEspacosPacotes(){
        return service.todosEspacosPacotes();
    }
    @PostMapping(value = "/adicionar")
    @ResponseBody
    public EspacosPacotes novoEspacoPacotes(@RequestBody EspacosPacotes espacosPacotes){
        return service.salvar(espacosPacotes);
    }
    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public EspacosPacotes editar(@PathVariable Integer id, @RequestBody EspacosPacotes espacosPacotes){
        espacosPacotes.setId(id);
        return service.editar(espacosPacotes,id);
    }
}
