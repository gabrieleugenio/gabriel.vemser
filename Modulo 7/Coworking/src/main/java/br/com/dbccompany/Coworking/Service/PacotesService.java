package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.PacotesEntity;
import br.com.dbccompany.Coworking.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.text.NumberFormat;

@Service
public class PacotesService {
    @Autowired
    private PacotesRepository repository;
    @Transactional(rollbackFor = Exception.class)
    public PacotesEntity salvar(PacotesEntity pacote){
        return repository.save(pacote);
    }
    @Transactional(rollbackFor = Exception.class)
    public PacotesEntity editar(PacotesEntity pacote,Integer id){
        pacote.setId(id);
        return repository.save(pacote);
    }
    public List<PacotesEntity> todosPacotes(){
        return (List<PacotesEntity>) repository.findAll();
    }
}
