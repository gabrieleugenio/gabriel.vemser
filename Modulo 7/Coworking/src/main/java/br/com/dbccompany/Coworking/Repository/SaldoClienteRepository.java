package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.SaldoCliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaldoClienteRepository  extends CrudRepository<SaldoCliente,Integer> {
}
