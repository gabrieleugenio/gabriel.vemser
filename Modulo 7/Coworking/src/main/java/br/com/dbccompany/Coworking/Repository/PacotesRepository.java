package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.PacotesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacotesRepository extends CrudRepository<PacotesEntity,Integer> {
}
