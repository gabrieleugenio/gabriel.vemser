package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.Coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TipoContatoService {
    @Autowired
    private TipoContatoRepository repository;
    @Transactional(rollbackFor = Exception.class)
    public TipoContatoEntity salvar(TipoContatoEntity tipoContato){
        return repository.save(tipoContato);
    }
    @Transactional(rollbackFor = Exception.class)
    public TipoContatoEntity editar(TipoContatoEntity tipoContato,Integer id){
        tipoContato.setId(id);
        return repository.save(tipoContato);
    }
    public List<TipoContatoEntity> todosTiposContato(){
        return (List<TipoContatoEntity>) repository.findAll();
    }
}
