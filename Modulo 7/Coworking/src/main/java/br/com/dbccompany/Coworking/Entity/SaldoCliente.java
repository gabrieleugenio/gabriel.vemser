package br.com.dbccompany.Coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class SaldoCliente {
    @EmbeddedId
    private SaldoClienteId id;

    @ManyToOne
    @MapsId("ID_CLIENTE")
    @JoinColumn(name = "ID_CLIENTE",nullable = false)
    private ClientesEntity cliente;

    @ManyToOne
    @MapsId("ID_ESPACO")
    @JoinColumn(name = "ID_ESPACO",nullable = false)
    private EspacosEntity espacos;

    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO_CONTRATACAO",nullable = false)
    private TipoContratacao tipoContratacao;

    @Column(name = "QUANTIDADE",nullable = false)
    private Double quantidade;

    @Column(name = "VENCIMENTO" ,nullable = false)
    private Date vencimento;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public EspacosEntity getEspaco() {
        return espacos;
    }

    public void setEspaco(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }
}
