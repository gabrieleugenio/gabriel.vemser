package br.com.dbccompany.Coworking.Controller;


import br.com.dbccompany.Coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.Coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/contratacoes")
public class ContratacaoController {
    @Autowired
    ContratacaoService service;
    @GetMapping(value = "/todas")
    @ResponseBody
    public List<ContratacaoEntity> todasContratacoes(){
        return service.todasContratacoes();
    }
    @PostMapping(value = "/adicionar")
    @ResponseBody
    public ContratacaoEntity novaContratacao(@RequestBody ContratacaoEntity contratacao){
        return service.salvar(contratacao);
    }
    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ContratacaoEntity editar(@PathVariable Integer id, @RequestBody ContratacaoEntity contratacao){
        contratacao.setId(id);
        return service.editar(contratacao,id);
    }
}
