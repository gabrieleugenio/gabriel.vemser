package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.ContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity,Integer> {
}
