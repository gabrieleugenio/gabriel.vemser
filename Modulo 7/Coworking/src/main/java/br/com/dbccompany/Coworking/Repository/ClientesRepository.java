package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.ClientesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientesRepository extends CrudRepository<ClientesEntity,Integer> {
}
