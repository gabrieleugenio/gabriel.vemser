package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.EspacosEntity;

import br.com.dbccompany.Coworking.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/espacos")
public class EspacosController {
    @Autowired
    EspacosService service;
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<EspacosEntity> todosEspacos(){
        return service.todosEspacos();
    }
    @PostMapping(value = "/adicionar")
    @ResponseBody
    public EspacosEntity novoEspaco(@RequestBody EspacosEntity espaco){
        return service.salvar(espaco);
    }
    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public EspacosEntity editar(@PathVariable Integer id, @RequestBody EspacosEntity espaco){
        espaco.setId(id);
        return service.editar(espaco,id);
    }
}
