package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.SaldoCliente;
import br.com.dbccompany.Coworking.Entity.SaldoClienteId;
import br.com.dbccompany.Coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SaldoClienteService {
    @Autowired
    private SaldoClienteRepository repository;
    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente salvar(SaldoCliente saldoCliente){
        return repository.save(saldoCliente);
    }
    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente editar(SaldoCliente saldoCliente, SaldoClienteId id){
        saldoCliente.setId(id);
        return repository.save(saldoCliente);
    }
    public List<SaldoCliente> todosSaldosClientes(){
        return (List<SaldoCliente>) repository.findAll();
    }

}
