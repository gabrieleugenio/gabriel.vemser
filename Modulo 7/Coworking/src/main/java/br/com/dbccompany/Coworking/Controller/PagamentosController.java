package br.com.dbccompany.Coworking.Controller;



import br.com.dbccompany.Coworking.Entity.PagamentosEntity;

import br.com.dbccompany.Coworking.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/pagamentos")
public class PagamentosController {
    @Autowired
    PagamentosService service;
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<PagamentosEntity> todosPagamentos(){
        return service.todosPagamentos();
    }
    @PostMapping(value = "/adicionar")
    @ResponseBody
    public PagamentosEntity novoPagamento(@RequestBody PagamentosEntity pagamento){
        return service.salvar(pagamento);
    }
    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public PagamentosEntity editar(@PathVariable Integer id, @RequestBody PagamentosEntity pagamento){
        pagamento.setId(id);
        return service.editar(pagamento,id);
    }
}
