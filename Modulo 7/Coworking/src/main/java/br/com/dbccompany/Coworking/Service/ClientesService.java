package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.ClientesEntity;
import br.com.dbccompany.Coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.Coworking.Repository.ClientesRepository;
import br.com.dbccompany.Coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientesService {
    @Autowired
    private ClientesRepository repository;
    private TipoContatoRepository repositoryTipo;

    @Transactional(rollbackFor = Exception.class)
    public ClientesEntity salvar(ClientesEntity cliente){
        TipoContatoEntity fone = new TipoContatoEntity();
        TipoContatoEntity email = new TipoContatoEntity();
        fone.setNome("Telefone");
        email.setNome("Email");
        return repository.save(cliente);
    }
    @Transactional(rollbackFor = Exception.class)
    public ClientesEntity editar(ClientesEntity cliente,Integer id){
        cliente.setId(id);
        return repository.save(cliente);
    }
    public List<ClientesEntity> todosClientes(){
        return (List<ClientesEntity>) repository.findAll();
    }
}
