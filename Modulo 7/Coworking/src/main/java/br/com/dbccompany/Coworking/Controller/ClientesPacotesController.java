package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.ClientesPacotes;
import br.com.dbccompany.Coworking.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientesPacotes")
public class ClientesPacotesController {
    @Autowired
    ClientesPacotesService service;

    @GetMapping(value = "/todas")
    @ResponseBody
    public List<ClientesPacotes> todosClientesPacotes(){
        return service.todosClientesPacotes();
    }

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public ClientesPacotes novoClientesPacotes(@RequestBody ClientesPacotes clientePacote){
        return service.salvar(clientePacote);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ClientesPacotes editar( @PathVariable Integer id, @RequestBody ClientesPacotes clientePacote){
        return service.editar(clientePacote,id);
    }
}
