package br.com.dbccompany.Coworking.Entity;

import javax.persistence.*;

@Entity
@Table(name = "ESPACOS_PACOTES")
public class EspacosPacotes {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACOS_PACOTES_SEQ", sequenceName="ESPACOS_PACOTES_SEQ")
    @GeneratedValue(generator = "ESPACOS_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ESPACOS_PACOTES", nullable= false)
    private Integer id;

    @Column(name = "QUANTIDADE",nullable = false)
    private Double quantidade;

    @Column(name = "PRAZO",nullable = false)
    private Integer prazo;
    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_ESPACOS", nullable = false )
    private EspacosEntity espacos;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_PACOTE", nullable = false )
    private PacotesEntity pacote;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public EspacosEntity getEspacos() {
        return espacos;
    }

    public void setEspaco(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public PacotesEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacotesEntity pacote) {
        this.pacote = pacote;
    }
}
