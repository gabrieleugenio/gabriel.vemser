package br.com.dbccompany.Coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PAGAMENTOS")
public class PagamentosEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PAGAMENTOS_SEQ", sequenceName="PAGAMENTOS_SEQ")
    @GeneratedValue(generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_PAGAMENTOS", nullable= false)
    private Integer id;
    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO_PAGAMENTO" , nullable= false)
    private TipoPagamento pagamento;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_CONTRATACAO")
    private ContratacaoEntity contratacao;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_CLIENTES_PACOTES")
    private ClientesPacotes clientesPacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamento getPagamento() {
        return pagamento;
    }

    public void setPagamento(TipoPagamento pagamento) {
        this.pagamento = pagamento;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public ClientesPacotes getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotes clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
