package br.com.dbccompany.Coworking.Entity;

import br.com.dbccompany.Coworking.Security.Criptografar;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Entity
@Table(name = "USUARIOS",uniqueConstraints={@UniqueConstraint(columnNames={"NOME","EMAIL","LOGIN"})})
public class Usuario {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "USUARIOS_SEQ", sequenceName="USUARIOS_SEQ")
    @GeneratedValue(generator = "USUARIOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_USUARIO", nullable= false)
    private Integer id;

    @Column(name = "NOME",nullable = false)
    private String nome;

    @Email
    @Column(name = "EMAIL",nullable = false)
    private String email;
    @Column(name = "LOGIN",nullable = false)
    private String login;

    @Size(min=6)
    @Column(name = "SENHA",nullable = false)
    private String senha;


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
