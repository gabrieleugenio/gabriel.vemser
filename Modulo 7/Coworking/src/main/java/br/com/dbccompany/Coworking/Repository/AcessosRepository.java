package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.Acessos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AcessosRepository  extends CrudRepository<Acessos, Integer> {
}
