package br.com.dbccompany.Coworking.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SaldoClienteId implements Serializable {
    @Column(name = "ID_CLIENTE",nullable = false)
    private Integer clienteId;

    @Column(name ="ID_ESPACO",nullable = false)
    private Integer espacoId;

    public SaldoClienteId() {
    }

    public SaldoClienteId(Integer clienteId, Integer espacoId) {
        this.clienteId = clienteId;
        this.espacoId = espacoId;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Integer getEspacoId() {
        return espacoId;
    }

    public void setEspacoId(Integer espacoId) {
        this.espacoId = espacoId;
    }
}
