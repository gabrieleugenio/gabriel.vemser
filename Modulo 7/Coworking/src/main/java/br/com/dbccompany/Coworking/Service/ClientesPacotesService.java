package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.ClientesPacotes;
import br.com.dbccompany.Coworking.Repository.ClientesPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class ClientesPacotesService {
    @Autowired
    private ClientesPacotesRepository repository;
    @Transactional(rollbackFor = Exception.class)
    public ClientesPacotes salvar(ClientesPacotes clientesPacotes){
        return repository.save(clientesPacotes);
    }
    @Transactional(rollbackFor = Exception.class)
    public ClientesPacotes editar(ClientesPacotes clientesPacotes, Integer id){
        clientesPacotes.setId(id);
        return repository.save(clientesPacotes);
    }
    public List<ClientesPacotes> todosClientesPacotes(){
        return (List<ClientesPacotes>) repository.findAll();
    }
}
