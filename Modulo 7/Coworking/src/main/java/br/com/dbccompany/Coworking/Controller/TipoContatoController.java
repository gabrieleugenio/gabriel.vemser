package br.com.dbccompany.Coworking.Controller;


import br.com.dbccompany.Coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.Coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipoContatos")
public class TipoContatoController {
    @Autowired
    TipoContatoService service;
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<TipoContatoEntity> todosTiposContatos(){
        return service.todosTiposContato();
    }
    @PostMapping(value = "/adicionar")
    @ResponseBody
    public TipoContatoEntity novoTipoContato(@RequestBody TipoContatoEntity tipoContato){
        return service.salvar(tipoContato);
    }
    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public TipoContatoEntity editar(@PathVariable Integer id, @RequestBody TipoContatoEntity tipoContato){
        tipoContato.setId(id);
        return service.editar(tipoContato,id);
    }
}
