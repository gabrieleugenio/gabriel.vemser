package br.com.dbccompany.Coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ACESSOS")
public class Acessos {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ACESSOS_SEQ", sequenceName="ACESSOS_SEQ")
    @GeneratedValue(generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ACESSOS", nullable= false)
    private Integer id;

    @Column(name = "DATA")
    private Date data;

    @Column(name = "IS_ENTRADA")
    private boolean isEntrada;

    @Column(name = "IS_EXCECAO")
    private boolean isExcecao;

    @ManyToOne
    @JoinColumns({
           @JoinColumn(
                  name = "ID_CLIENTES_SALDO_CLIENTE",
                   referencedColumnName = "ID_CLIENTE",
                  nullable = false
           ),
            @JoinColumn(
                   name = "ID_ESPACO_SALDO_CLIENTE",
                    referencedColumnName = "ID_ESPACO",
                  nullable = false
            )
                })
    private SaldoCliente saldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }
}
