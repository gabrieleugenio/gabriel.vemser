package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.Acessos;
import br.com.dbccompany.Coworking.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/acessos")
public class AcessosController {
    @Autowired
    AcessosService service;
    @GetMapping(value = "/todas")
    @ResponseBody
    public List<Acessos> todosAcessos(){
        return service.todosAcessos();
    }
    @PostMapping( value = "/adicionar")
    @ResponseBody
    public Acessos novoAcesso(@RequestBody Acessos acesso){
        return service.salvar(acesso);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public Acessos editar( @PathVariable Integer id, @RequestBody Acessos acesso){
        acesso.setId(id);
        return service.editar(acesso,id);
    }

}
