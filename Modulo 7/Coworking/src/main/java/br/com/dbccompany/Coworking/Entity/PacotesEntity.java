package br.com.dbccompany.Coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PACOTES")
public class PacotesEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PACOTES_SEQ", sequenceName="PACOTES_SEQ")
    @GeneratedValue(generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_PACOTES", nullable= false)
    private Integer id;

    @Column(name = "VALOR", nullable= false)
    private Double valor;

    @OneToMany(mappedBy = "pacote")
    private List<EspacosPacotes> espacosPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "pacote")
    private List<ClientesPacotes> clientesPacotes = new ArrayList<>();

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
//        String brl = "R$";
//        this.valor = brl.concat(valor);
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<EspacosPacotes> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotes> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }
}
