package br.com.dbccompany.Coworking.Controller;


import br.com.dbccompany.Coworking.Entity.ContatoEntity;
import br.com.dbccompany.Coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contatos")
public class ContatoController {
    @Autowired
    ContatoService service;
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ContatoEntity> todosContatos(){
        return service.todosContatos();
    }
    @PostMapping(value = "/adicionar")
    @ResponseBody
    public ContatoEntity novoContato(@RequestBody ContatoEntity contato){
        return service.salvar(contato);
    }
    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ContatoEntity editar(@PathVariable Integer id, @RequestBody ContatoEntity contato){
        contato.setId(id);
        return service.editar(contato,id);
    }
}
