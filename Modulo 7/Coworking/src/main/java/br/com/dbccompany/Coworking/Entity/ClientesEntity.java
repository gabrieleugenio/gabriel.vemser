package br.com.dbccompany.Coworking.Entity;



import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "CLIENTE")
public class ClientesEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName="CLIENTE_SEQ")
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CLIENTE", nullable= false)
    private Integer id;

    @Column(name = "NOME",nullable= false)
    private String nome;

    @CPF
    @Column(name = "CPF",unique = true,nullable= false)
    private String cpf;

    @Column(name = "DATA_NASCIMENTO",nullable= false)
    private Date dataNascimento;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_CONTATO" )
    private ContatoEntity contato;
    @OneToMany(mappedBy = "cliente")
    private  List<SaldoCliente> saldoClientes = new ArrayList<>();
    @OneToMany(mappedBy = "cliente")
    private List<ClientesPacotes> clientesPacotes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public ContatoEntity getContato() {
        return contato;
    }

    public void setContato(ContatoEntity contato) {
        this.contato = contato;
    }
}
