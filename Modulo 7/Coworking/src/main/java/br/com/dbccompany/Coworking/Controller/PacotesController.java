package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.PacotesEntity;
import br.com.dbccompany.Coworking.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/pacotes")
public class PacotesController {
    @Autowired
    PacotesService service;
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<PacotesEntity> todosPacotes(){
        return service.todosPacotes();
    }
    @PostMapping(value = "/adicionar")
    @ResponseBody
    public PacotesEntity novoPacote(@RequestBody PacotesEntity pacote){
        return service.salvar(pacote);
    }
    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public PacotesEntity editar(@PathVariable Integer id, @RequestBody PacotesEntity pacote){
        pacote.setId(id);
        return service.editar(pacote,id);
    }
}
