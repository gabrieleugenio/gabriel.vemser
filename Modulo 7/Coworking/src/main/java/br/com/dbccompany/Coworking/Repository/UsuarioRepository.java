package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario,Integer> {
}
