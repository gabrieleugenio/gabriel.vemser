package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.ContatoEntity;
import br.com.dbccompany.Coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContatoService {
    @Autowired
    private ContatoRepository repository;
    @Transactional(rollbackFor = Exception.class)
    public ContatoEntity salvar(ContatoEntity contato){
        return repository.save(contato);
    }
    @Transactional(rollbackFor = Exception.class)
    public ContatoEntity editar(ContatoEntity contato,Integer id){
        contato.setId(id);
        return repository.save(contato);
    }
    public List<ContatoEntity> todosContatos(){
        return (List<ContatoEntity>) repository.findAll();
    }
}
