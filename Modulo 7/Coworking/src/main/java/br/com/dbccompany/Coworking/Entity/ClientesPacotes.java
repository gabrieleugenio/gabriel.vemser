package br.com.dbccompany.Coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CLIENTES_PACOTES")
public class ClientesPacotes {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTES_PACOTES_SEQ", sequenceName="CLIENTES_PACOTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CLIENTES_PACOTES", nullable= false)
    private Integer id;

    @Column(name = "QUANTIDADE",nullable = false)
    private Double quantidade;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_CLIENTE", nullable = false )
    private ClientesEntity cliente;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_PACOTE", nullable = false )
    private PacotesEntity pacote;

    @OneToMany(mappedBy = "clientesPacotes")
    private List<PagamentosEntity> pagamentos = new ArrayList<>();

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public PacotesEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacotesEntity pacote) {
        this.pacote = pacote;
    }
}
