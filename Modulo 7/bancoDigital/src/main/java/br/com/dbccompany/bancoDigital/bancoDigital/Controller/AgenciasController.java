package br.com.dbccompany.bancoDigital.bancoDigital.Controller;

import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Agencias;
import br.com.dbccompany.bancoDigital.bancoDigital.Service.AgenciasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/agencias")
public class AgenciasController {
    @Autowired
    AgenciasService service;

    @GetMapping( value = "/todas")
    @ResponseBody
    public List<Agencias> todasAgencias(){
        return service.todasAgencias();
    }

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public Agencias novaAgencia(@RequestBody Agencias agencia){
        return service.salvar(agencia);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public Agencias editar( @PathVariable Integer id, @RequestBody Agencias agencia){
        return service.editar(agencia,id);
    }

    @GetMapping( value = "/busca")
    @ResponseBody
    public Agencias agenciaEspecifica(Integer id){
        return service.agenciaEspecifica( id);
    }
}
