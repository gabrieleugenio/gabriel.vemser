package br.com.dbccompany.bancoDigital.bancoDigital.Entity;

import javax.persistence.*;

@Entity
@Table(name="ESTADOS")
public class Estados {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESTADOS_SEQ", sequenceName="ESTADOS_SEQ")
    @GeneratedValue(generator = "ESTADOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name="ID_ESTADO",nullable= false)
    private Integer id;
    private String nome;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_PAIS",nullable= false)
    private Paises pais;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Paises getPais() {
        return pais;
    }

    public void setPais(Paises pais) {
        this.pais = pais;
    }
}
