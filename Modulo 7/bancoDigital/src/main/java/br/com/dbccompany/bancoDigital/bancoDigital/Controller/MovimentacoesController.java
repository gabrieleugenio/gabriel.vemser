package br.com.dbccompany.bancoDigital.bancoDigital.Controller;

import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Movimentacoes;
import br.com.dbccompany.bancoDigital.bancoDigital.Service.MovimentacoesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/movimentacoes" )
public class MovimentacoesController {

    @Autowired
    MovimentacoesService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Movimentacoes> buscarTodas() {
        return service.todasMovimentacoes();
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public Movimentacoes buscarId( @PathVariable Integer id ) {
        return service.buscaPorId(id);
    }


    @PostMapping( value = "/nova" )
    @ResponseBody
    public Movimentacoes adicionar(@RequestBody Movimentacoes movimentacao ) {
        return service.salvar(movimentacao);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Movimentacoes editar( @PathVariable Integer id, @RequestBody Movimentacoes movimentacao ) {
        return service.editar(movimentacao, id);
    }
}
