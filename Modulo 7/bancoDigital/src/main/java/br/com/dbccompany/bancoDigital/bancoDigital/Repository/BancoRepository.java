package br.com.dbccompany.bancoDigital.bancoDigital.Repository;

import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Banco;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BancoRepository extends CrudRepository<Banco, Integer> {
    List<Banco> findAllByNome( String nome );
}
