package br.com.dbccompany.bancoDigital.bancoDigital.Entity;
import javax.persistence.*;

@Entity
@Table(name="MOVIMENTACOES")
public class Movimentacoes {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "MOVIMENTACOES_SEQ", sequenceName="MOVIMENTACOES_SEQ")
    @GeneratedValue(generator = "MOVIMENTACOES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", nullable= false)
    private Integer id;
    private Double valor;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="FK_ID_CONTA", nullable= false)
    private Contas conta;

    @Enumerated(EnumType.STRING)
    private Tipo tipo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Contas getConta() {
        return conta;
    }

    public void setConta(Contas conta) {
        this.conta = conta;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }
}
