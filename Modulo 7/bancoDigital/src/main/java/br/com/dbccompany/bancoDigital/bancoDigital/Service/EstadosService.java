package br.com.dbccompany.bancoDigital.bancoDigital.Service;


import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Estados;
import br.com.dbccompany.bancoDigital.bancoDigital.Repository.EstadosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EstadosService {

    @Autowired
    private EstadosRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Estados salvar( Estados estado ) {
        return repository.save(estado);
    }

    @Transactional( rollbackFor = Exception.class )
    public Estados editar( Estados estado, Integer id ) {
        estado.setId(id);
        return  repository.save(estado);
    }

    public List<Estados> todosEstados() {
        return (List<Estados>) repository.findAll();
    }

    public Estados buscaPorId(Integer id ) {
        return repository.findById(id).get();
    }

}
