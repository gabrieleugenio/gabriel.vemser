package br.com.dbccompany.bancoDigital.bancoDigital.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CIDADES")
public class Cidades {

    @Id
    @SequenceGenerator(name = "CIDADES_SEQ", allocationSize = 1, sequenceName = "CIDADES_SEQ")
    @GeneratedValue(generator = "CIDADES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Column(name = "NOME", nullable = false)
    private String nome;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "FK_ID_ESTADO",nullable = false)
    private Estados estado;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "clientes_x_cidades",
            joinColumns = { @JoinColumn( name = "id_cidade" ) },
            inverseJoinColumns = {@JoinColumn( name = "id_cliente" ) })
    private List<Clientes> clientes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
