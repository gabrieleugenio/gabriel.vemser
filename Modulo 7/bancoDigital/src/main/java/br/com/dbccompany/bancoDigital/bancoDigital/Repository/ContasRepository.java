package br.com.dbccompany.bancoDigital.bancoDigital.Repository;

import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Contas;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContasRepository extends CrudRepository<Contas, Integer> {
}
