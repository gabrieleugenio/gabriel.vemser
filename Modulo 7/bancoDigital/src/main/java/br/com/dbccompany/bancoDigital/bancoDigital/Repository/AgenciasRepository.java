package br.com.dbccompany.bancoDigital.bancoDigital.Repository;

import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Agencias;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AgenciasRepository extends CrudRepository<Agencias, Integer> {
    Optional<Agencias> findByNome(String nome );
}
