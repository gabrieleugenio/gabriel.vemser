package br.com.dbccompany.bancoDigital.bancoDigital.Controller;

import br.com.dbccompany.bancoDigital.bancoDigital.Entity.TipoConta;
import br.com.dbccompany.bancoDigital.bancoDigital.Service.TipoContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/tipoconta" )
public class TipoContaController {


    @Autowired
    TipoContaService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<TipoConta> buscarTodas() {
        return service.todosTiposContas();
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public TipoConta buscarId( @PathVariable Integer id ) {
        return service.buscaPorId(id);
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public TipoConta adicionar( @RequestBody TipoConta tipoConta ) {
        return service.salvar(tipoConta);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public TipoConta editar(@PathVariable Integer id, @RequestBody TipoConta tipoConta ) {
        return service.editar(tipoConta, id);
    }
}
