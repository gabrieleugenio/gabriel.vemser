package br.com.dbccompany.bancoDigital.bancoDigital.Repository;

import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Movimentacoes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovimentacoesRepository extends CrudRepository<Movimentacoes, Integer> {
}
