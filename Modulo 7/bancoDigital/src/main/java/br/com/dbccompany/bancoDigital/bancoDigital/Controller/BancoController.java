package br.com.dbccompany.bancoDigital.bancoDigital.Controller;

import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Banco;
import br.com.dbccompany.bancoDigital.bancoDigital.Service.BancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/banco")
public class BancoController {
    @Autowired
    BancoService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Banco> todosBancos(){
        return service.todosBancos();
    }

    @PostMapping( value = "/adicionar" )
    @ResponseBody
    public Banco novoBanco(@RequestBody Banco banco){
        return service.salvar(banco);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public Banco editar(@PathVariable Integer id, @RequestBody Banco banco){
        return  service.editar(banco, id);
    }

    @GetMapping(value = "/busca")
    @ResponseBody
    public List<Banco> findAllByNome(String nome){
        return service.findAllByNome(nome);
    }
}
