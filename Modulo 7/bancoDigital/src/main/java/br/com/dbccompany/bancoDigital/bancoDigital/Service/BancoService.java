package br.com.dbccompany.bancoDigital.bancoDigital.Service;


import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Banco;
import br.com.dbccompany.bancoDigital.bancoDigital.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BancoService {
    @Autowired
    private BancoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Banco salvar( Banco banco){
        return repository.save(banco);
    }
    @Transactional(rollbackFor = Exception.class)
    public Banco editar( Banco banco, Integer codigo){
        banco.setCodigo(codigo);
        return repository.save(banco);
    }
    public List<Banco> todosBancos(){
        return (List<Banco>) repository.findAll();
    }
    public Banco bancoEspecifico(Integer codigo){
        Optional<Banco> banco = repository.findById(codigo);
        return banco.get();
    }
    public List<Banco> findAllByNome(String nome){
        return (List<Banco>) repository.findAllByNome(nome);
    }
}
