package br.com.dbccompany.bancoDigital.bancoDigital.Controller;

import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Cidades;
import br.com.dbccompany.bancoDigital.bancoDigital.Service.CidadesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/cidades")
public class CidadeController {
    @Autowired
    CidadesService service;

    @GetMapping( value = "/todas")
    @ResponseBody
    public List<Cidades> todasCidades(){
        return service.todasCidades();
    }
    @PostMapping( value = "/adicionar")
    @ResponseBody
    public Cidades novaCidade(@RequestBody Cidades cidade){
        return service.salvar(cidade);
    }
    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public Cidades editar(@PathVariable Integer id, @RequestBody Cidades cidade){
        return service.editar(cidade, id);
    }
    @GetMapping(value = "/busca")
    @ResponseBody
    public Cidades findByNome(String nome) {
        return service.findByNome(nome);
    }
}
