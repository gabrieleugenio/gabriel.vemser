package br.com.dbccompany.bancoDigital.bancoDigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CLIENTES")
public class Clientes {

    @Id
    @SequenceGenerator(name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Column(name = "CPF", nullable = false)
    private Integer cpf;

    @Column(name = "NOME", nullable = false)
    private String nome;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "clientes_x_cidades",
            joinColumns = { @JoinColumn( name = "id_cliente" ) },
            inverseJoinColumns = {@JoinColumn( name = "id_cidade" ) })
    private List<Cidades> cidades = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCpf() {
        return cpf;
    }

    public void setCpf(Integer cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
