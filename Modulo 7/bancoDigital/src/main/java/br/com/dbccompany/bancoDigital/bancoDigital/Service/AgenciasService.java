package br.com.dbccompany.bancoDigital.bancoDigital.Service;

import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Agencias;
import br.com.dbccompany.bancoDigital.bancoDigital.Repository.AgenciasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AgenciasService {
    @Autowired
    private AgenciasRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Agencias salvar(Agencias agencia){
        return repository.save(agencia);
    }
    @Transactional(rollbackFor = Exception.class)
    public Agencias editar(Agencias agencia, Integer codigo){
        agencia.setCodigo(codigo);
        return repository.save(agencia);
    }
    public List<Agencias> todasAgencias(){
        return (List<Agencias>) repository.findAll();
    }
    public Agencias agenciaEspecifica(Integer codigo){
        Optional<Agencias> agencia = repository.findById(codigo);
        return agencia.get();
    }
    public Agencias findByNome(String nome){
        Optional<Agencias> agencia = repository.findByNome(nome);
        return agencia.get();
    }
}
