package br.com.dbccompany.bancoDigital.bancoDigital.Repository;

import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Estados;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadosRepository extends CrudRepository<Estados, Integer> {

}
