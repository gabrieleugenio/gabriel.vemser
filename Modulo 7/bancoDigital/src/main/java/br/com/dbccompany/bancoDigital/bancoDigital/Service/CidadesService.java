package br.com.dbccompany.bancoDigital.bancoDigital.Service;

import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Cidades;
import br.com.dbccompany.bancoDigital.bancoDigital.Repository.CidadesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
public class CidadesService {
    @Autowired
    private CidadesRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Cidades salvar( Cidades cidade ){
        return repository.save(cidade);
    }
    @Transactional( rollbackFor = Exception.class)
    public Cidades editar(Cidades cidade, Integer id){
        cidade.setId(id);
        return repository.save(cidade);
    }
    public List<Cidades> todasCidades(){
        return (List<Cidades>) repository.findAll();
    }
    public Cidades findByNome(String nome){
        Optional<Cidades> cidade = repository.findByNome(nome);
        return cidade.get();
    }
}
