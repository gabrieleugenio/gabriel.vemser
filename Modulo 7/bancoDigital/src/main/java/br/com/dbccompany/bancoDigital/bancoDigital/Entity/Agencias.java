package br.com.dbccompany.bancoDigital.bancoDigital.Entity;

import javax.persistence.*;

@Entity
@Table(name="AGENCIAS")
public class Agencias {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "AGENCIAS_SEQ", sequenceName="AGENCIAS_SEQ")
    @GeneratedValue(generator = "AGENCIAS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name="CODIGO",nullable= false)
    private Integer codigo;
    private String nome;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="FK_ID_CIDADE",nullable= false)
    private Cidades cidade;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="FK_COD_BANCO",nullable= false)
    private Banco banco;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Cidades getCidade() {
        return cidade;
    }

    public void setCidade(Cidades cidade) {
        this.cidade = cidade;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }
}
