package br.com.dbccompany.bancoDigital.bancoDigital.Service;


import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Clientes;
import br.com.dbccompany.bancoDigital.bancoDigital.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientesService {

    @Autowired
    private ClientesRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Clientes salvar(Clientes cliente ) {
        return repository.save(cliente);
    }

    @Transactional( rollbackFor = Exception.class)
    public Clientes editar( Clientes cliente, Integer id ) {
        cliente.setId( id );
        return repository.save(cliente);
    }

    public List<Clientes> todosClientes() {
        return (List<Clientes>) repository.findAll();
    }

    public Clientes buscaPorId( Integer id ) {
        return repository.findById(id).get();
    }

}
