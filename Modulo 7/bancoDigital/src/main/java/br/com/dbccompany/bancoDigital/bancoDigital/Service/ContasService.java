package br.com.dbccompany.bancoDigital.bancoDigital.Service;

import br.com.dbccompany.bancoDigital.bancoDigital.Entity.Contas;
import br.com.dbccompany.bancoDigital.bancoDigital.Repository.ContasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContasService {

    @Autowired
    private ContasRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Contas salvar(Contas conta ) {
        return repository.save(conta);
    }

    @Transactional( rollbackFor = Exception.class)
    public Contas editar( Contas conta, Integer id ) {
        conta.setId( id );
        return repository.save(conta);
    }

    public List<Contas> todasContas() {
        return (List<Contas>) repository.findAll();
    }

    public Contas buscaPorId(Integer id ) {
        return repository.findById(id).get();
    }

}
